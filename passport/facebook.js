var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../models/user');
var fbConfig = {
  'appID' : '1568156983478843',
  'appSecret' : 'f43460cbcf234ffd30c722d8cac4422e',
  'callbackUrl' : '/login/facebook/callback'
};

module.exports = function(passport) {

  // Tell passport to use facebook to login
  passport.use('facebook',

    // Creating a new Facebook Strategy
    new FacebookStrategy(

      {
        clientID        : fbConfig.appID,
        clientSecret    : fbConfig.appSecret,
        callbackURL     : fbConfig.callbackUrl,
        profileFields   : ["id", "email", "first_name", "last_name"]
      },

      // facebook will send back the tokens and profile
      function(access_token, refresh_token, profile, done) {

        // Executing code asynchronously
        process.nextTick(function() {
          User.findOne({ 'fb.id' : profile.id }, function(err, user) {
            if (err) return done(err);

            // if the user is found, then log them in
            if (user) {
              console.log('User (' + user.fb.firstName + ' ' + user.fb.lastName + ') has been found');
              return done(null, user);
            } else {
              // Creating a new user
              var newUser = new User();

              // set all of the facebook information in our user model
              newUser.fb.id    = profile.id; // set the users facebook id                 
              newUser.fb.access_token = access_token; // we will save the token that facebook provides to the user                  
              newUser.fb.firstName  = profile.name.givenName;
              newUser.fb.lastName = profile.name.familyName; // look at the passport user profile to see how names are returned
              newUser.fb.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
              newUser.cloak.currentSessionId = profile.id;
              newUser.cloak.initialJoin = true;
              newUser.save(function(err) {
                if (err)
                  throw err;
                return done(null, newUser);
              });
            }
          });
        });
      }
    )
  );
};
