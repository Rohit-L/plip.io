var User = require('../models/user');
var Game = require('../models/game');
var network = require('./network/index');
var UUID = require('node-uuid');

var MIN_ROOM_MEMBERS = 1;
var MAX_ROOM_MEMBERS = 2;
var WALL_TILE = 2;
var BOARD_SIZE = 25;
var GRID_SIZE = 64;
var MAX_BOMBS = 20;

network.configure({
  express: server,
  gameLoopSpeed: 15,
  defaultRoomSize: MAX_ROOM_MEMBERS,
  autoCreateRooms: true,
  minRoomMembers: MIN_ROOM_MEMBERS,
  pruneEmptyRooms: 1000,
  reconnectWait: 0,
  messages: {

    connect: connectUser,

    requestIDs: sendIDs,

    readyToPlay: function(arg, user) {
      user.getRoom().data['readyToPlay'] += 1
    },

    goBackToLobby: function(arg, user) {
      user.leaveRoom();
    },

    gameIsActuallyPlaying: function(arg, user) {
      var room = user.getRoom();
      room.data['gameActuallyPlaying'] = true;
      room.data.info[user.id]['actuallyPlaying'] = true

      room.startTimer = setTimeout(function(data) {
        data[0].start();
      }, 5000, [room.world])
      room.messageMembers('preparingRoom');

    },

    readyButtonPressed: function(arg, user) {
      network.getLobby().addMember(user);
    },

    input: function(data, user) {
      var inputs = {
        0: "spacebarDown",
        1: "spacebarUp",
        2: "bKeyDown",
        3: "bKeyUp",
        4: "vKeyDown",
        5: "vKeyUp",
        6: "upKeyDown",
        7: "downKeyDown",
        8: "leftKeyDown",
        9: "rightKeyDown",
        10: "cKeyDown",
        11: "cKeyUp"
      }
      var input = inputs[data.readUInt8(0)];
      var tank = user.getRoom().world.tanksMap[user.id];

      if (input === "spacebarDown") { tank.fire = true; }
      if (input === "spacebarUp") { tank.fire = false; }
      if (input === "bKeyDown") { tank.placeBomb = true; }
      if (input === "bKeyUp") { tank.placeBomb = false; }
      if (input === "vKeyDown") { tank.fireMegaBullet = true; }
      if (input === "vKeyUp") { tank.fireMegaBullet = false; }
      if (input === "upKeyDown") { tank.futureDirection = Directions.UP; }
      if (input === "downKeyDown") { tank.futureDirection = Directions.DOWN; }
      if (input === "leftKeyDown") { tank.futureDirection = Directions.LEFT; }
      if (input === "rightKeyDown") { tank.futureDirection = Directions.RIGHT; }
      if (input === "cKeyDown") { tank.activateShield = true; }
      if (input === "cKeyUp") { tank.activateShield = false; }
    }

  },
  room: {
    init: function() {
      // Set initial data
      this.data.readyToPlay = 0;
      this.data.playing = false;
      this.data.info = {};
      this.data.numBots = 4;

      this.tick = 0;

      var possibleMaps = ['1', '2', '3', '4', '5', '6', '7', '8'];
      this.data.map = 'map' + 
          possibleMaps[Math.floor(Math.random() * possibleMaps.length)] + '.json';

      this.data.bombPositions = getBombPositions(this.data.map);

      for (var i = 0; i < this.data.numBots; i++) {
        createBot(this, i);
      }

    },
    pulse: function() {
      if (this.data.playing) {
        if (this.data.gameActuallyPlaying === true) {
          if (this.world) { 
            this.tick += 1;

            if (this.tick % 1 === 0) {
              // Send Tank Movements
              for (var i = 0; i < this.world.tanks.length; i++) {
                var tank = this.world.tanks[i];
                if (tank.isAlive()) {
                  var data = new Buffer(6);
                  data.writeUInt16BE(tank.state.pos.x, 0);
                  data.writeUInt16BE(tank.state.pos.y, 2);
                  data.writeUInt8(tank.facing, 4);
                  data.writeUInt8(tank.moving, 5);
                  this.messageMembers('tankUpdate', Buffer.concat([Buffer.from(tank.id), data]));
                }
              }

              // Send to client if they die -- contains last assaulter
              for (var i = 0; i < this.world.deadTankUpdates.length; i++) {
                var tank = this.world.deadTankUpdates[i];
                var user = this.network.getUser(tank.id)
                if (user) {
                  if (tank.lastAssaulter) {
                    user.message('deadTankUpdate', Buffer.from(tank.lastAssaulter.id));
                  } else {
                    user.message('deadTankUpdate');
                  }
                }
              }
              this.world.deadTankUpdates = [];

              // Send Bullet Movements
              for (var i = 0; i < this.world.bullets.length; i++) {
                var bullet = this.world.bullets[i];
                if (bullet) {
                  var data = new Buffer(7);
                  data.writeUInt16BE(bullet.state.pos.x, 0);
                  data.writeUInt16BE(bullet.state.pos.y, 2);
                  data.writeUInt16BE(bullet.uid, 4);
                  data.writeUInt8(bullet.direction, 6);
                  this.messageMembers('bulletUpdate', data);
                }
              }

              // Send to all clients when bullet is dead (collides)
              for (var i = 0; i < this.world.deadBullets.length; i++) {
                var bullet = this.world.deadBullets[i];
                var data = new Buffer(2);
                data.writeUInt16BE(bullet.uid, 0);
                this.messageMembers('killBullet', data);
              }
              this.world.deadBullets = [];

              // Send Mega-Bullet Movements
              for (var i = 0; i < this.world.megaBullets.length; i++) {
                var megaBullet = this.world.megaBullets[i];
                var data = new Buffer(7);
                data.writeUInt16BE(megaBullet.state.pos.x, 0);
                data.writeUInt16BE(megaBullet.state.pos.y, 2);
                data.writeUInt16BE(megaBullet.uid, 4);
                data.writeUInt8(megaBullet.direction, 6);
                this.messageMembers('megaBulletUpdate', data);              
              }

              // Send to all clients when Mega-Bullet is dead (collides)
              for (var i = 0; i < this.world.deadMegaBullets.length; i++) {
                var megaBullet = this.world.deadMegaBullets[i];
                var data = new Buffer(2);
                data.writeUInt16BE(megaBullet.uid, 0);
                this.messageMembers('killMegaBullet', data);
              }
              this.world.deadMegaBullets = [];

              // Send to all clients when a bomb is picked up
              for (var i = 0; i < this.world.pickedUpBombs.length; i++) {
                var bomb = this.world.pickedUpBombs[i];
                var data = new Buffer(2);
                data.writeUInt16BE(bomb.uid, 0);
                this.messageMembers('pickedUpBomb', data);
              }
              this.world.pickedUpBombs = [];

              // Send to all clients when a bomb is activated
              for (var i = 0; i < this.world.placedBombs.length; i++) {
                var placedBomb = this.world.placedBombs[i];
                var data = new Buffer(4);
                data.writeUInt16BE(placedBomb[0], 0);
                data.writeUInt16BE(placedBomb[1], 2);
                this.messageMembers('placedBomb', data);
              }
              this.world.placedBombs = [];


              // Send to all clients when a wall gets damaged
              for (var i = 0; i < this.world.damagedWalls.length; i++) {
                var wall = this.world.damagedWalls[i];
                var data = new Buffer(6);
                data.writeUInt16BE(wall.tileX, 0);
                data.writeUInt16BE(wall.tileY, 2);
                data.writeUInt16BE(wall.health, 4);
                this.messageMembers('damageWall', data);
              }
              this.world.damagedWalls = [];

              // Send to all clients when a tank's score changed
              for (var i = 0; i < this.world.scoreUpdates.length; i++) {
                var tank = this.world.scoreUpdates[i];
                var data = new Buffer(2);
                data.writeUInt16BE(tank.score, 0);
                this.messageMembers('scoreUpdate', Buffer.concat([Buffer.from(tank.id), data]));
              }
              this.world.scoreUpdates = [];

              // Send to all clients when a tank's health changes
              for (var i = 0; i < this.world.healthUpdates.length; i++) {
                var tank = this.world.healthUpdates[i];
                var data = new Buffer(2);
                data.writeUInt8(tank.health, 0);
                this.messageMembers('healthUpdate', Buffer.concat([Buffer.from(tank.id), data]));
              }
              this.world.healthUpdates = [];

              // Send to client when streak changes
              for (var i = 0; i < this.world.streakUpdates.length; i++) {
                var tank = this.world.streakUpdates[i];
                var data = new Buffer(2);
                data.writeUInt16BE(tank.streak, 0);
                var user =  this.network.getUser(tank.id)
                if (user) { user.message('streakUpdate', data); }
              }
              this.world.streakUpdates = [];

              // Send to client when the client's bomb count changes
              for (var i = 0; i < this.world.bombCountUpdates.length; i++) {
                var tank = this.world.bombCountUpdates[i];
                var data = new Buffer(2);
                data.writeUInt8(tank.numBombs, 0);
                var user =  this.network.getUser(tank.id)
                if (user) { user.message('bombCountUpdate', Buffer.concat([Buffer.from(tank.id), data])); }
              }
              this.world.bombCountUpdates = [];

              for (var i = 0; i < this.world.megaBulletCountUpdates.length; i++) {
                var tank = this.world.megaBulletCountUpdates[i];
                var data = new Buffer(2);
                data.writeUInt8(tank.numMegaBullets, 0);
                var user =  this.network.getUser(tank.id)
                if (user) { user.message('megaBulletCountUpdate', Buffer.concat([Buffer.from(tank.id), data])); }
              }
              this.world.megaBulletCountUpdates = [];

              for (var i = 0; i < this.world.notifyTimeOut.length; i++) {
                var tank = this.world.notifyTimeOut[i];
                var user = this.network.getUser(tank.id);
                if (user) { user.message('timeOut'); }
              }
              this.world.notifyTimeOut = [];

              for (var i = 0; i < this.world.notifyWonGame.length; i++) {
                var tank = this.world.notifyWonGame[i];
                var user = this.network.getUser(tank.id);
                if (user) { user.message('wonGame'); }
              }
              this.world.notifyWonGame = [];

              for (var i = 0; i < this.world.actionUpdates.length; i++) {
                var update = JSON.stringify(this.world.actionUpdates[i]);
                this.messageMembers('actionUpdate', Buffer.from(update));
              }
              this.world.actionUpdates = [];

              for (var i = 0; i < this.world.shieldUpdates.length; i++) {
                var tank = this.world.shieldUpdates[i];
                var data = new Buffer(1);
                data.writeUInt8(tank.numShields, 0);
                this.messageMembers('shieldActivated', Buffer.concat([Buffer.from(tank.id), data]));
              }
              this.world.shieldUpdates = [];
            }            
          }
          return;
        }
      } else if (this.data.readyToPlay >= MAX_ROOM_MEMBERS) {
        
        this.world = require('../physics/index.js')(this);

        // Construct string for player id/nicknames
        var memberIDs = Object.keys(this.data.info);
        var dataString = "";
        for (var i = 0; i < memberIDs.length; i++) {
          dataString += memberIDs[i];
          dataString += this.data.info[memberIDs[i]].nickname
          dataString += '<|>'
        }
        this.messageMembers('startGame', Buffer.from(dataString));

        // Construct string for bomb positions
        var dataString = "";
        for (var id in this.world.bombsMap) {
          var bomb = this.world.bombsMap[id];
          dataString += bomb.state.pos.x + "|" + bomb.state.pos.y + "|" + id
          dataString += "<|>";
        }
        this.messageMembers('bombPositions', Buffer.from(dataString));

        // Tell clients to start game and send player nicknames
        this.messageMembers('mapSelection', Buffer.from(this.data.map));
        this.data.playing = true

        var members = this.getMembers();
        for (var i = 0; i < members.length; i++) {
          var user = members[i]
          setTimeout(function(args) {
            var room = args[0]; var user = args[1]
            if (room.data.info) {
              if (!room.data.info[user.id]['actuallyPlaying']) {
                user.leaveRoom()
              }
            }
          }, 10000, [this, user])
        }

      }
    },
    newMember: function(user) {
      // Add user to state and set initial variables
      this.data.info[user.id] = {};
      this.data.info[user.id]['isBot'] = false;
      this.data.info[user.id]['score'] = 0;
      this.data.info[user.id]['health'] = 20;
      this.data.info[user.id]['bombsUsed'] = 0;
      this.data.info[user.id]['bulletsShot'] = 0;
      this.data.info[user.id]['hitsTaken'] = 0;
      this.data.info[user.id]['bulletsHitOtherPlayer'] = 0;
      this.data.info[user.id]['actuallyPlaying'] = false;
    },

    memberLeaves: function(user) {
      if (this.data['gameActuallyPlaying']) {
        if (!this.world.gameOver) {
          var tank = this.world.tanksMap[user.id];
          tank.destroy();
          this.world.healthUpdates.push(tank);
        }
        compileUserData(user, this.data['info'][user.id]);
        updateHighestScore(this.data['info'][user.id]);
      }
    },

    close: function() { 
      if (this.data['gameActuallyPlaying']) {
        updateWinnerInDB(this.data['info']);
        updateGameCount();
      }

      if (this.world) {
        this.world.pause();
        this.world.stop();
        clearInterval(this.world.livingRewardTimer);
        clearTimeout(this.world.gameTimeout);
      }
      
      delete this.data['info'];
   }
  },

  lobby: {

    init: function() {
      this.data.tick = 0;
      this.data.info = { 'minRoomMembers': MAX_ROOM_MEMBERS };
      Game.findOne({ 'name': 'Tank Warfare' }, function(err, game) {
        if (!game) {
          var newGame = new Game();
          newGame.name = 'Tank Warfare';
          newGame.save(function(err) { if (err) throw err; });
        }
      });     
    },

    pulse: function() {
      this.data.tick += 1;
      this.data.info['lobbyLength'] = this.getMembers().length;
      this.data.info['roomCount'] = network.roomCount();
      this.data.info['playerCount'] = getPlayerCount(network.getRooms(true));
      if (this.data.tick % 60 === 0) { sendGameStats(this.data.info); }
    },

    newMember: function(user) {
      if (!user.connected()) { user.delete(); }
    },
  }
});
network.run();

function getPlayerCount(rooms) {
  var count = 0;
  for (var i = 0; i < rooms.length; i++) {
    var room = rooms[i];
    count += room.users.length;
  }
  return count;
}

function updateGameCount() {
  Game.findOne({ 'name': 'Tank Warfare' }, function(err, game) {
    if (game) {
      game.stats.totalGamesPlayed += 1;
      game.save(function(err) { if (err) throw err; });
    }
  });
}

function sendGameStats(gameData) {
  Game.findOne({ 'name': 'Tank Warfare' }, function(err, game) {
    if (game) {
      sendGameStatistics(game, gameData);
    }
  });
}

function sendGameStatistics(game, gameData) {
  data = new Buffer(12);
  var users = network.getUsers();
  data.writeUInt16BE(game.stats.highestScore, 0);
  data.writeUInt16BE(game.stats.totalGamesPlayed, 2);
  data.writeUInt16BE(gameData['minRoomMembers'], 4);
  data.writeUInt16BE(gameData['lobbyLength'], 6);
  data.writeUInt16BE(gameData['roomCount'], 8);
  data.writeUInt16BE(gameData['playerCount'], 10);
  for (var i = 0; i < users.length; i++) {
    users[i].message('gameStats', data);
  }
}

function updateHighestScore(info) {
  Game.findOne({ 'name': 'Tank Warfare' }, function(err, game) {
    if (game) {
      if (game.stats.highestScore < info['score']) {
        game.stats.highestScore = info['score'];
      }
      game.save(function(err) { if (err) throw err; });
    }
  });
}

function compileUserData(user, info) {
  User.findOne({ 'cloak.currentSessionId': user.id }, function(err, dbUser) {
    if (dbUser) {
      dbUser.stats.totalScore += info['score'];
      dbUser.stats.hitsTaken += info['hitsTaken'];
      dbUser.stats.bulletsShot += info['bulletsShot'];
      dbUser.stats.bulletsHitOtherPlayer += info['bulletsHitOtherPlayer'];
      dbUser.stats.bombsUsed += info['bombsUsed'];
      dbUser.stats.gamesPlayed += 1;
      if (dbUser.stats.highScore < info['score']) {
        dbUser.stats.highScore = info['score'];
      }
      if (user) {
        sendStatistics(user, dbUser)
      }
      dbUser.save(function(err) { if (err) throw err; });
    }
  });
}

function updateWinnerInDB(info) {
  var maxScore = -1;
  var winners = [];
  for (var uid in info) { // get all players with max score
    if (info.hasOwnProperty(uid)) {
      var score = info[uid]['score'];
      if (score > maxScore) {
        maxScore = score;
        winners = [uid];
      } else if (score == maxScore) {
        winners.push(uid);
      }
    }
  }
  var i = 0;
  while (i < winners.length) {
    User.findOne({ 'cloak.currentSessionId': winners[i] }, function(err, dbUser) {
      if (dbUser) {
        dbUser.stats.gamesWon += 1;
        dbUser.save(function(err) { if (err) throw err; });
      }
    });
    i++;
  }
}

function getBombPositions(map) {
  // Read map data into server
  var fs = require('fs')
  var mapData = JSON.parse(fs.readFileSync("./public/data/" + map, 'utf8'))
  var tileData = mapData["layers"][0]["data"]

  // Calculate and set bomb positions
  var i = 0; var min = 1; var max = BOARD_SIZE - 1;
  var bombPositions = [];
  while (i < MAX_BOMBS) {
    tileX = getRandomIntInclusive(min, max);
    tileY = getRandomIntInclusive(min, max);
    if (tileData[(25 * tileY) + tileX] == WALL_TILE) {
      continue;
    }
    x = tileX * GRID_SIZE; y = tileY * GRID_SIZE;
    bombPositions.push([x, y]);
    i++;
  }
  return bombPositions;
}

function getRandomIntInclusive(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createBot(room, botNumber) {
  id = UUID.v4();
  room.data.info[id] = {};
  room.data.info[id]['isBot'] = true;
  room.data.info[id]['score'] = 0;
  room.data.info[id]['health'] = 20;
  room.data.info[id]['bombsUsed'] = 0;
  room.data.info[id]['bulletsShot'] = 0;
  room.data.info[id]['hitsTaken'] = 0;
  room.data.info[id]['bulletsHitOtherPlayer'] = 0;
  room.data.info[id]['actuallyPlaying'] = false;
  room.data.info[id]['nickname'] = "Bot " + botNumber;
}

function sendIDs(nicknameBuffer, user) {
  /* Client sends nickname when requesting IDs */
  var nickname = nicknameBuffer.toString();
  var room = user.getRoom();
  room.data.info[user.id].nickname = nickname;

  var memberIDs = Object.keys(room.data.info);
  var membersBuffer = new Buffer(0);
  for (var i = 0; i < memberIDs.length; i++) {
    membersBuffer = Buffer.concat([membersBuffer, Buffer.from(memberIDs[i])]);
  }
  user.message('responseIDs', membersBuffer);
}

function sendStatistics(user, dbUser) {
  data = new Buffer(16);

  data.writeUInt16BE(dbUser.stats.gamesPlayed, 0);
  data.writeUInt16BE(dbUser.stats.totalScore, 2);
  data.writeUInt16BE(dbUser.stats.gamesWon, 4);
  data.writeUInt16BE(dbUser.stats.hitsTaken, 6);
  data.writeUInt16BE(dbUser.stats.bulletsShot, 8);
  data.writeUInt16BE(dbUser.stats.bulletsHitOtherPlayer, 10);
  data.writeUInt16BE(dbUser.stats.bombsUsed, 12);
  data.writeUInt16BE(dbUser.stats.highScore, 14);
  user.message('statistics', data);
}

function connectUser(data, user) {
  id = data.toString()
  User.findOne({ 'fb.id': id }, function(err, dbUser) {
    if (dbUser) {
      dbUser.cloak.currentSessionId = user.id;
      sendStatistics(user, dbUser)
      dbUser.save(function(err) { if (err) throw err; });
    }
  });
}

// for debugging
function deleteUsers() {
  User.find({}, function(err, users) {
    i = 0
    while (i < users.length) {
      users[i].remove(function(err) {
      });
      i++
    }
  });
}
