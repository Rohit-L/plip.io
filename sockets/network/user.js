/* jshint node:true */

var _ = require('underscore');
var uuid = require('node-uuid');

module.exports = (function() {

  function User(network, socket, data) {
    this.network = network;
    this.id = socket.id;
    this._socket = socket;
    this.name = 'Nameless User';
    this.disconnectedSince = null;
    this.data = data || {};
  }

  User.prototype = {

    message: function(name, arg) {
      var message = new Buffer(1)
      message.writeUInt8(clientKeyCodes['message-' + name])
      
      var data;
      if (arg) {
        data = Buffer.concat([message, arg])
      } else {
        data = message;
      }

      this._socket.transmit(data);
    },

    _serverMessage: function(name, arg) {
      var message = new Buffer(1); 
      message.writeUInt8(clientKeyCodes['network-' + name])
      var data = Buffer.concat([message, arg])

      this._socket.transmit(data);
    },

    leaveRoom: function() {
      if (this.room !== undefined) {
        this.room.removeMember(this);
      }
    },

    joinRoom: function(room) {
      room.addMember(this);
    },

    getRoom: function() {
      return this.room;
    },

    connected: function() {
      return this.disconnectedSince === null;
    },

    _userData: function() {
      return {
        id: this.id,
        name: this.name
      };
    },

    delete: function() {
      this.disconnectedSince = this.disconnectedSince || new Date().getTime();
      this.leaveRoom();
      this._socket.close();
      this.network._deleteUser(this);
    }

  };

  return User;

})();
