var WebSocketServer = require('ws').Server
var _ = require('underscore');
var uuid = require('node-uuid');

var User = require('./user.js');
var Room = require('./room.js');

module.exports = (function() {

  var users = {};
  var rooms = {};
  var socketIdToUserId = {};
  var io;
  var gameLoopInterval;
  var lobby;
  var roomNum = 0;
  keyCodes = { '0': 'network-begin',
               '1': 'message-connect',
               '2': 'message-requestIDs',
               '3': 'message-readyToPlay',
               '4': 'message-getBombPositions',
               '5': 'message-gameIsActuallyPlaying',
               '6': 'message-myTankInfo',
               '7': 'message-requestTurn',
               '8': 'message-turnCompleted',
               '9': 'message-bombPlaced',
               '10': 'message-bulletFired',
               '11': 'message-bulletInteraction',
               '12': 'message-goBackToLobby',
               '13': 'message-readyButtonPressed',
               '14': 'message-requestShield',
               '15': 'message-tileDestroyingNeeded',
               '17': 'message-transferBots',
               '16': 'message-input' } // incoming
  clientKeyCodes = { 'network-beginResponse': 0, 
                     'network-lobbyMemberJoined': 1, 
                     'network-joinedRoom': 2,
                     'network-roomMemberJoined': 4,
                     'network-lobbyMemberLeft': 5,
                     'network-roomMemberLeft': 6,
                     'network-roomCreated': 7,
                     'network-roomDeleted': 8,
                     'message-statistics': 9,
                     'message-responseIDs': 10,
                     'message-startGame': 11,
                     'message-bombPositions': 12,
                     'network-leftRoom': 13,
                     'message-turnApproved': 14,
                     'message-oppositeTurnIsValid': 15,
                     'message-bombPlaced': 16,
                     'message-bulletFired': 17,
                     'message-tankUpdate': 18,
                     'message-mapSelection': 19,
                     'message-gameStats': 20,
                     'message-activateShield': 23,
                     'message-destroyTile': 24,
                     'message-botHost': 25,
                     'message-bulletUpdate': 26,
                     'message-killBullet': 27,
                     'message-pickedUpBomb': 28,
                     'message-placedBomb': 29,
                     'message-damageWall': 30,
                     'message-megaBulletUpdate': 31,
                     'message-killMegaBullet': 32,
                     'message-scoreUpdate': 33,
                     'message-healthUpdate': 34,
                     'message-bombCountUpdate': 35,
                     'message-megaBulletCountUpdate': 36,
                     'message-deadTankUpdate': 37,
                     'message-timeOut': 38,
                     'message-wonGame': 39,
                     'message-actionUpdate': 40,
                     'message-streakUpdate': 41,
                     'message-timeUpdate': 42,
                     'message-shieldActivated': 43,
                     'message-preparingRoom': 44 } // outgoing

  var defaults = {
    port: 8090,
    gameLoopSpeed: 100,
    defaultRoomSize: null,
    autoCreateRooms: false,
    minRoomMembers: null,
    reconnectWait: 10000,
    pruneEmptyRooms: null,
    roomLife: null,
    autoJoinLobby: false,
    notifyRoomChanges: true
  };

  var config;
  var events;

  var network = {

    // configure the server
    configure: function(configArg) {

      config = _.extend({}, defaults);
      events = {};

      _(configArg).forEach(function(val, key) {
        if (key === 'room' ||
            key === 'lobby') {
          events[key] = val;
        }
        else {
          config[key] = val;
        }
      });
    },

    // run the server
    run: function() {

      io = new WebSocketServer({ server: config.express });

      if (config.express) {
        console.log(('Network is running with Express server on port ' +
                    config.express.address().port).info);
      }
      else {
        console.log(('network running on port ' + config.port).info);
      }

      // We won't want to try to serialize this later
      if (config.express) {
        delete config.express;
      }

      lobby = new Room(network, 'Lobby', 0, events.lobby, true);

      Room.prototype._lobby = lobby;
      Room.prototype._autoJoinLobby = config.autoJoinLobby;
      Room.prototype._minRoomMembers = config.minRoomMembers;

      io.on('connection', function(socket) {
        socket.id = uuid.v4();
        console.log((socket.id + ' connected').info);

        socket.transmit = function(data) {
          if (socket.readyState === 1) {
            // console.log("==========")            
            // console.log("OUTPUT: " + data.readUInt8(0));
            // console.log(data.toString())
            // console.log("==========")
            socket.send(data);
          }
        }

        socket.messages = {}

        socket.on('message', function incoming(data, flags) {
          var keyCode = data.readUInt8(0);
          // console.log(socket.id + " " + keyCodes[keyCode]);
          socket.messages[keyCodes[keyCode]](data.slice(1))
        });

        socket.on('close', function() {
          var uid = socketIdToUserId[socket.id];
          var user = network._getUser(uid);
          if (!user) {
            return;
          }
          user.disconnectedSince = new Date().getTime();
          delete socketIdToUserId[socket.id];
          if (config.clientEvents && config.clientEvents.disconnect) {
            config.clientEvents.disconnect(user);
          }
          console.log((socket.id + ' has disconnected').info);
          user.delete();
        });

        // network-begin
        socket.messages['network-begin'] = function(data) {
          var user = new User(network, socket, data);
          users[user.id] = user;
          socketIdToUserId[socket.id] = user.id;
          network._setupHandlers(socket);

          // Sending network-beginResponse
          var message = new Buffer(1);
          message.writeUInt8(clientKeyCodes['network-beginResponse'])
          var data = Buffer.concat([message, Buffer.from(user.id)])
          socket.transmit(data);

          console.log((socket.id + ' begins').info);
          if (config.autoJoinLobby) {
            user.joinRoom(lobby);
          }
          if (config.clientEvents && config.clientEvents.begin) {
            config.clientEvents.begin(user);
          }
        };

        socket.messages['network-resume'] = function(data) {
          var uid = data.uid;
          var user = users[uid];
          if (user !== undefined) {
            socketIdToUserId[socket.id] = uid;
            user._socket = socket;
            user.disconnectedSince = null;
            network._setupHandlers(socket);
            socket.transmit('network-resumeResponse', {
              valid: true,
              config: config
            });
            if (config.clientEvents && config.clientEvents.resume) {
              config.clientEvents.resume(user);
            }
            console.log((socket.id + ' resumes').info);
          }
          else {
            socket.transmit('network-resumeResponse', {valid: false});
            console.log((socket.id + ' fails to resume').info);
          }
        };

      });

      gameLoopInterval = setInterval(function() {
        var room;

        // Pulse lobby
        lobby._pulse();

        // Pulse all rooms
        _(rooms).forEach(function(room) {
          var oldEnoughToPrune = room.members.length < 1 && new Date().getTime() - room._lastEmpty >= config.pruneEmptyRooms;
          var roomExpired = config.roomLife !== null && new Date().getTime() - room.created >= config.roomLife;

          if (roomExpired) {
            room.delete();
          }
          else if (config.pruneEmptyRooms && oldEnoughToPrune) {
            room.delete();
          }
          else {
            room._pulse();
          }
        });

        var membersAvailable = config.defaultRoomSize !== null && lobby.members.length >= config.defaultRoomSize;

        // autoCreateRooms
        if (config.autoCreateRooms && membersAvailable) {
          roomNum++;
          room = network.createRoom('Room ' + roomNum);
          _.range(config.defaultRoomSize).forEach(function(i) {
            room.addMember(lobby.members[0]);
          });
        }

        // Prune rooms with member counts below minRoomMembers
        if (config.minRoomMembers !== null) {
          _(rooms).forEach(function(room) {
            if (room._hasReachedMin && room.members.length < config.minRoomMembers) {
              room.delete();
            }
          });
        }

      }, config.gameLoopSpeed);

    },

    _setupHandlers: function(socket) {

      _(config.messages).each(function(handler, name) {
        socket.messages['message-' + name] = function(arg) {
          var user = network._getUserForSocket(socket);
          try {
            handler(arg, user);
          }
          catch (error) {
            console.error('Uncaught error in message handler for "' + name + '"');
            console.error(error);
          }
        };
      });

    },

    getRooms: function(json) {
      if (json) {
        return _.invoke(rooms, '_roomData');
      }
      else {
        return _.values(rooms);
      }
    },

    createRoom: function(name, size) {
      var roomName = name || 'Nameless Room';
      var roomSize = size || config.defaultRoomSize;
      var room = new Room(network, roomName, roomSize, events.room, false, config.minRoomMembers);
      rooms[room.id] = room;
      if (config.notifyRoomChanges) {
        // Message everyone in lobby
        data = new Buffer(2)
        data.writeUInt16BE(network.roomCount(), 0)
        lobby._serverMessageMembers('roomCreated', data);
      }
      return room;
    },

    _deleteRoom: function(room) {
      delete rooms[room.id];
      if (config.notifyRoomChanges) {
        data = new Buffer(2)
        data.writeUInt16BE(network.roomCount(), 0)
        lobby._serverMessageMembers('roomDeleted', data);
      }
    },

    getRoom: function(id) {
      return rooms[id] || false;
    },

    getLobby: function() {
      return lobby;
    },

    _deleteUser: function(user) {
      delete users[user.id];
    },

    _getUidForSocket: function(socket) {
      return socketIdToUserId[socket.id];
    },

    _getUserForSocket: function(socket) {
      return this._getUser(this._getUidForSocket(socket));
    },

    _getUser: function(uid) {
      return users[uid];
    },

    userCount: function() {
      return _(users).size();
    },

    roomCount: function() {
      return _(rooms).size();
    },

    getUser: function(id) {
      return users[id] || false;
    },

    getUsers: function(json) {
      if (json) {
        return _.invoke(users, '_userData');
      }
      else {
        return _.values(users);
      }
    },

    messageAll: function(name, arg) {
      _(users).forEach(function(user) {
        user.message(name, arg);
      });
    },

    stop: function(callback) {

      // Stop the game loop
      clearInterval(gameLoopInterval);

      // Delete all users
      _(users).forEach(function(user) {
        user.delete();
      });

      // Delete all rooms
      _(rooms).forEach(function(room) {
        room.delete();
      });

      // Shut down socket server
      if (io) {
        try {
          io.server.close();
          callback();
        }
        catch(e) {
          callback();
        }
      }
      else {
        callback();
      }
    },

    // For testing
    _getIo: function() {
      return io;
    }

  };

  return network;

})();
