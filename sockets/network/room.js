/* jshint node:true */
var _ = require('underscore');
var uuid = require('node-uuid');

module.exports = (function() {

  function Room(network, nameArg, sizeArg, roomEventsArg, isLobby, minRoomMembers) {
    this.network = network;
    this._roomEvents = roomEventsArg || {};
    this.isLobby = isLobby;
    this.minRoomMembers = minRoomMembers;
    this.id = uuid.v4();
    this.name = nameArg;
    this.members = [];
    this.size = sizeArg;
    this.created = new Date().getTime();
    this.data = {};
    this._emitEvent('init', this);
    this._lastEmpty = new Date().getTime();

    console.log((nameArg + ' created.').roomInfo);
  }

  Room.prototype = {

    _pulse: function() {
      this._emitEvent('pulse', this);
    },

    // return true if successful
    addMember: function(user) {
      if (!this._shouldAllowUser(user)) {
        return false;
      }
      user.leaveRoom();
      this.members.push(user);
      user.room = this;
      if (this.minRoomMembers !== null &&
          this.members.length >= this.minRoomMembers) {
        this._hasReachedMin = true;
      }
      this._emitEvent('newMember', this, user);
      this._serverMessageMembers(this.isLobby ? 'lobbyMemberJoined' : 'roomMemberJoined', Buffer.from(user.id));
      user._serverMessage('joinedRoom', Buffer.from(this.name));

      console.log((user.id + ' joined ' + this.name + '.').userInfo);
      console.log(("Members in " + this.name + ": " + this.getMembers().length).roomInfo);
      return true;
    },

    removeMember: function(user) {
      if (user.room !== this) {
        return;
      }
      this.members = _(this.members).without(user);
      delete user.room;
      if (this.members.length < 1) {
        this._lastEmpty = new Date().getTime();
      }
      if (!this.isLobby && this._autoJoinLobby) {
        this._lobby.addMember(user);
      }
      this._emitEvent('memberLeaves', this, user);
      this._serverMessageMembers(this.isLobby ? 'lobbyMemberLeft' : 'roomMemberLeft', Buffer.from(user.id));
      user._serverMessage('leftRoom', Buffer.from(this.name));

      console.log((user.id + ' left ' + this.name + '.').userInfo);
      console.log(("Members in " + this.name + ": " + this.getMembers().length).roomInfo);
    },

    age: function() {
      return new Date().getTime() - this.created;
    },

    getMembers: function(json) {
      if (json) {
        return _.invoke(this.members, '_userData');
      }
      else {
        return _.values(this.members);
      }
    },

    messageMembers: function(name, arg) {
      _.forEach(this.members, function(member) {
        member.message(name, arg);
      }.bind(this));
    },

    delete: function() {
      this._closing = true;
      _(this.members).forEach(function(user) {
        user.leaveRoom();
      });
      this._emitEvent('close', this);
      this.network._deleteRoom(this);

      console.log((this.name + ' has closed').roomInfo);
    },

    _serverMessageMembers: function(name, arg) {
      _.forEach(this.members, function(member) {
        member._serverMessage(name, arg);
      }.bind(this));
    },

    _shouldAllowUser: function(user) {
      if (this._roomEvents.shouldAllowUser) {
        return this._emitEvent('shouldAllowUser', this, user);
      }
      else {
        return true;
      }
    },

    _emitEvent: function(event, context, args) {
      var roomEvent = this._roomEvents[event];
      if (args !== undefined && !Array.isArray(args)) {
        args = [args];
      }
      if (!!roomEvent) {
        return roomEvent.apply(context, args);
      }
    },

    _roomData: function() {
      return {
        id: this.id,
        name: this.name,
        users: _.map(this.members, function(user) {
          return user._userData();
        }),
        size: this.size
      };
    }

  };

  return Room;

})();
