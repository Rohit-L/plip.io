var mongoose = require('mongoose');

module.exports = mongoose.model('Game', {
  name: String,
  stats: {
    totalGamesPlayed: { type: Number, default: 0 },
    highestScore: {type: Number, default: 0 }
  }
});
