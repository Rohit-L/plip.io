var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
  fb: {
    id: { type: String, unique: true },
    access_token: String,
    firstName: String,
    lastName: String,
    email: String
  },
  stats: {
    gamesPlayed: { type: Number, default: 0 },
    totalScore: { type: Number, default: 0 },
    gamesWon: { type: Number, default: 0 },
    hitsTaken: { type: Number, default: 0 },
    bulletsShot: { type: Number, default: 0 },
    bulletsHitOtherPlayer: { type: Number, default: 0 },
    bombsUsed: { type: Number, default: 0 },
    highScore: { type: Number, default: 0 }
  },
  cloak: {
    currentSessionId: String
  }
});
