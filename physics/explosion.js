module.exports = function(Physics) {

  Physics.addExplosion = function(tank, origin, time) {
    var EXPLOSION_RADIUS = 96;
    var EXPLOSION_SCORE_INCREASE = 2;
    var EXPLOSION_HEALTH_DECREASE = 2;

    setTimeout(function(data) {
      // Loop through all tanks and decrease health for each once
      var world = data[0];
      var origin = data[1];
      var originalTank = data[2];
      var tanks = world.aliveTanks;

      for (var i = 0; i < tanks.length; i++) {
        var tank = tanks[i];
        var x = tank.state.pos.x;
        var y = tank.state.pos.y;
        var distance = Math.sqrt((x - origin.x)*(x - origin.x) + (y - origin.y)*(y - origin.y))
        if (distance < EXPLOSION_RADIUS && !tank.shielded) {
          tank.reduceHealth(EXPLOSION_HEALTH_DECREASE, originalTank);
          originalTank.increaseScore(EXPLOSION_SCORE_INCREASE);
          originalTank.increaseStreak(1);
          world.actionUpdates.push([originalTank.id, tank.id, 'bombed'])
        }
      }

      // Reduce health of surrounding walls
      var tileX = Math.floor(origin.x / 64);
      var tileY = Math.floor(origin.y / 64);
      var coordinates = [
        [tileX - 1, tileY - 1], [tileX, tileY - 1], [tileX + 1, tileY - 1],
        [tileX - 1, tileY], [tileX, tileY], [tileX + 1, tileY],
        [tileX - 1, tileY + 1], [tileX, tileY + 1], [tileX + 1, tileY + 1]
      ]
      for (var i = 0; i < coordinates.length; i++) {
        var coordinate = coordinates[i];
        var wall = world.wallsMap[coordinate[0] + "," + coordinate[1]]
        if (wall) { wall.reduceHealth(); }
      }

    }, time, [tank.world, origin, tank]);
  }
}
