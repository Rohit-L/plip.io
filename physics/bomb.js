module.exports = function(Physics) {

  Physics.addBomb = function(world, x, y) {
    var bomb = Physics.body('rectangle', {
      x: x + 32,
      y: y + 32,
      width: 52,
      height: 52,
      treatment: 'static'
    });
    bomb.name = 'bomb'
    bomb.world = world

    bomb._collide = function(otherBody) {
      if (otherBody.name === 'tank') {
        otherBody.numBombs += 1;
        this.world.bombCountUpdates.push(otherBody);
        this.world.remove(this);
        delete this.world.bombsMap[this.uid];
        this.world.pickedUpBombs.push(this);
      }
    }
    
    world.add(bomb);
    return bomb
  }
}
