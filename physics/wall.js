module.exports = function(Physics) {
  
  Physics.addWall = function(world, tileX, tileY) {
    var wall = Physics.body('rectangle', {
      x: (64 * tileX) + 32,
      y: (64 * tileY) + 32,
      width: 64,
      height: 64,
      treatment: 'static'
    });
    wall.name = 'wall'
    wall.health = 3;
    wall.world = world;
    wall.tileX = tileX;
    wall.tileY = tileY;

    wall._collide = function(otherBody) {
      if (otherBody.name === 'bullet') {
        this.reduceHealth()
      }
    }

    wall.reduceHealth = function() {
      if (this.tileX === 0 || this.tileX === 24 || this.tileY === 0 || this.tileY === 24) {
        return;
      }
      
      this.health -= 1;
      if (this.health === 0) {
        this.world.remove(this);
        this.world.walls.splice(this.world.walls.indexOf(this), 1);
        delete this.world.wallsMap[this.tileX + "," + this.tileY];
      }
      this.world.damagedWalls.push(this);
    }

    world.add(wall);
    return wall
  }
}
