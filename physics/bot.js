module.exports = function() {
  var helpers = {}

  helpers.setTarget = function(tank, world) {
    if (world.gameOver) { return; }
    if (world.room.data.info) {
      var allIDs = Object.keys(world.room.data.info);
      var id = allIDs[Math.floor(Math.random() * allIDs.length)]
      tank.bot.target = world.tanksMap[id];
      while (tank.bot.target === tank || !tank.bot.target.isAlive()) {
        id = allIDs[Math.floor(Math.random() * allIDs.length)];
        tank.bot.target = world.tanksMap[id];
      }
    }
  }

  helpers.chooseAction = function(tank, world) {
    if (tank.bot.mode === "OFFENSE") {
      if (helpers.chaseTarget(tank, world)) { return; }
    }

    if (tank.bot.mode === "DEFENSE") {
      if (helpers.runFromTarget(tank, world)) { return; }
    }

    if (tank.bot.mode === "MIDDLE") {
      if (helpers.goToMiddle(tank, world)) { return; }
    }

    if (!tank.moving) {
      var arr = [];
      var threshold = 0.4;
      if (!tank._rightTile()) { arr.push(Directions.RIGHT); }
      if (!tank._belowTile()) { arr.push(Directions.DOWN); }
      if (!tank._aboveTile()) { arr.push(Directions.UP); }
      if (!tank._leftTile()) { arr.push(Directions.LEFT); }
      return tank.futureDirection = arr[Math.floor(Math.random() * arr.length)];
    }
  }

  helpers.runFromTarget = function(tank, world) {
    var x = tank.state.pos.x;
    var y = tank.state.pos.y;
    var targetX = tank.bot.target.state.pos.x;
    var targetY = tank.bot.target.state.pos.y;
    var yDistance = Math.abs(y - targetY);
    var xDistance = Math.abs(x - targetX);

    if (yDistance >= xDistance) {
      if (x - targetX > 0) {
        if (!tank._rightTile()) {
          tank.futureDirection = Directions.RIGHT
          return true
        }
      } else {
        if (!tank._leftTile()) {
          tank.futureDirection = Directions.LEFT
          return true
        }
      }
    }

    if (xDistance >= yDistance) {
      if (y - targetY > 0) {
        if (!tank._belowTile()) {
          tank.futureDirection = Directions.DOWN
          return true
        }
      } else {
        if (!tank._aboveTile()) {
          tank.futureDirection = Directions.UP
          return true
        }
      }
    }

    return false    
  }

  helpers.chaseTarget = function(tank, world) {
    var x = tank.state.pos.x;
    var y = tank.state.pos.y;
    var targetX = tank.bot.target.state.pos.x;
    var targetY = tank.bot.target.state.pos.y;
    var yDistance = Math.abs(y - targetY);
    var xDistance = Math.abs(x - targetX);

    if (xDistance >= yDistance) {
      if (x - targetX > 0) {
        if (!tank._leftTile()) {
          tank.futureDirection = Directions.LEFT
          return true
        }
      } else {
        if (!tank._rightTile()) {
          tank.futureDirection = Directions.RIGHT
          return true
        }
      }
    }

    if (yDistance >= xDistance) {
      if (y - targetY > 0) {
        if (!tank._aboveTile()) {
          tank.futureDirection = Directions.UP
          return true
        }
      } else {
        if (!tank._belowTile()) {
          tank.futureDirection = Directions.DOWN
          return true
        }
      }
    }

    return false
  }

  helpers.goToMiddle = function(tank, world) {
    var middle = 800;
    var yDistance = Math.abs(y - middle);
    var xDistance = Math.abs(x - middle);

    if (xDistance >= yDistance) {
      if (x - middle > 0) {
        if (!tank._leftTile()) {
          tank.futureDirection = Directions.LEFT
          return true
        }
      } else {
        if (!tank._rightTile()) {
          tank.futureDirection = Directions.RIGHT
          return true
        }
      }
    }

    if (yDistance >= xDistance) {
      if (y - middle > 0) {
        if (!tank._aboveTile()) {
          tank.futureDirection = Directions.UP
          return true
        }
      } else {
        if (!tank._belowTile()) {
          tank.futureDirection = Directions.DOWN
          return true
        }
      }
    }

    return false
  }

  return function(tank, world) {
    tank.bot.counter += 1;

    if (!tank.bot.targetTank) {
      helpers.setTarget(tank, world);
    }

    if (tank.bot.counter % 400 === 0) {
      helpers.setTarget(tank, world);

      var rand = Math.random();
      if (rand <= 0.4) {
        tank.bot.mode = "OFFENSE";
      } else if (rand <= 0.8) {
        tank.bot.mode = "DEFENSE";
      } else {
        tank.bot.mode = "MIDDLE";
      }
    }

    if (tank.bot.counter % 10 === 0) {
      helpers.chooseAction(tank, world);
    }

    if (tank.bot.counter % 5 === 0) {
      tank.fire = Math.random() > 0.800
    }

  }

}
