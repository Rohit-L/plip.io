module.exports = function(Physics) {
  
  Physics.addBullet = function(world, tank) {
    var bullet = Physics.body('rectangle', {
      x: tank.state.pos.x,
      y: tank.state.pos.y,
      width: 12,
      height: 12
    });
    bullet.name = 'bullet';
    bullet.tank = tank;
    bullet.world = world;
    bullet.direction = Directions.NONE;

    var speed = 2.5;
    if (tank.facing === Directions.LEFT) { 
      bullet.state.vel.x = -speed;
      bullet.direction = Directions.LEFT
    }
    if (tank.facing === Directions.RIGHT) { 
      bullet.state.vel.x = speed;
      bullet.direction = Directions.RIGHT;
    }
    if (tank.facing === Directions.UP) {
      bullet.state.vel.y = -speed;
      bullet.direction = Directions.UP;
    }
    if (tank.facing === Directions.DOWN) {
      bullet.state.vel.y = speed;
      bullet.direction = Directions.DOWN;
    }
    
    bullet._collide = function(otherBody) {
      if (otherBody.name === 'wall' || (otherBody.name === 'tank' && this.tank.id !== otherBody.id)) {
        this.world.remove(this);
        this.world.bullets.splice(this.world.bullets.indexOf(this), 1);
        this.world.deadBullets.push(this);
      }
    }
    
    world.add(bullet)
    world.bullets.push(bullet);
    return bullet
  }
}
