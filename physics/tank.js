module.exports = function(Physics) {
  
  Physics.addTank = function(world, x, y, id, isBot) {
    var tank = Physics.body('rectangle', {
      x: x + 32,
      y: y + 32,
      width: 48,
      height: 48
    })

    tank.TANK_SPEED = 0.9;
    tank.name = 'tank';
    tank.id = id;
    tank.world = world;
    tank.isBot = isBot;
    if (tank.isBot) {
      tank.bot = {};
      tank.bot.counter = 0;
      tank.bot.mode = "OFFENSE";
      tank.bot.target = null;
    }

    tank.previousTurnPoint = { x: null, y: null };
    tank.turnPoint = { x: null, y: null };
    tank.collisionHandled = false;
    tank.currentDirection = Directions.NONE;
    tank.futureDirection = Directions.NONE;
    tank.facing = Directions.UP;
    tank.moving = false;
    tank.lastAssaulter = null;

    tank.fire = false;
    tank.lastFire = null;

    tank.fireMegaBullet = false;
    tank.lastFireMegaBullet = null;

    tank.placeBomb = false;
    tank.lastBomb = null;

    tank.activateShield = false;
    tank.lastShield = null;
    tank.shielded = false;

    tank.health = 20;
    tank.score = 0;
    tank.streak = 0;
    tank.numBombs = 0;
    tank.numMegaBullets = 5;
    tank.numShields = 2;

    /******************
     * Public Methods *
     ******************/
    tank.refresh = function() {
      this._calculateTurnPoint();
      if (this._canTurn()) { this._turn(); }
      if (this.fire && this._canFire()) { this._fire(); }
      if (this.fireMegaBullet && this._canFireMegaBullet()) { this._fireMegaBullet(); }
      if (this.placeBomb && this._canPlaceBomb()) { this._placeBomb(); }
      if (this.activateShield && this._canActivateShield()) { this._activateShield(); }
    }

    tank.increaseScore = function(amount) {
      this.score += amount;
      if (this.score < 0) {
        this.score = 0;
      }
      this.world.scoreUpdates.push(tank);
      if (this.world.room.data.info) {
        this.world.room.data.info[this.id].score = this.score;
      }
    }

    tank.reduceHealth = function(amount, assaultingTank) {
      if (this.shielded) { return; }
      this.lastAssaulter = assaultingTank;
      this.health -= amount;
      if (this.health <= 0) {
        this.destroy()
      }
      this.world.healthUpdates.push(tank);
    }

    tank.increaseStreak = function(amount) {
      this.streak += amount;
      this.world.streakUpdates.push(tank);
      if (this.streak > 0 && this.streak % 10 === 0) {
        this.addMegaBullet(5);
        this.increaseScore(5);
      }
    }

    tank.addMegaBullet = function(amount) {
      this.numMegaBullets += amount;
      this.world.megaBulletCountUpdates.push(this);
    }

    tank.isAlive = function() {
      return this.health > 0
    }
   
    tank.destroy = function() {
      console.log(this.id + " has died! (Final Score: " + this.score + ")"); 

      this.health = 0;
      var index = this.world.aliveTanks.indexOf(this);
      if (index >= 0) {
        this.world.aliveTanks.splice(index, 1);
        this.world.remove(this);
        this.world.deadTankUpdates.push(this);
      }
    }

     /*******************
     * Private Methods *
     *******************/
    tank._collide = function(otherBody) {

      if (otherBody.name === 'bullet') {
        
        if (this.shielded) { return; }

        if (otherBody.tank.id !== this.id && this.world.room.data.info) {
          this.reduceHealth(1, otherBody.tank);
          this.world.room.data.info[this.id].hitsTaken += 1;
          otherBody.tank.increaseScore(2);
          otherBody.tank.increaseStreak(1);
          this.world.room.data.info[otherBody.tank.id].bulletsHitOtherPlayer += 1;
          this.world.actionUpdates.push([otherBody.tank.id, this.id, 'shot']);
        }
        return;
      }

      if (otherBody.name === 'wall') {
        this._stop(this.turnPoint.x, this.turnPoint.y);
      }

      if (otherBody.name === 'tank') {
        if (this.collisionHandled) {
          this.collisionHandled = false;
          return;
        }
        if (this.turnPoint.x === otherBody.turnPoint.x && 
                        this.turnPoint.y === otherBody.turnPoint.y) {
          this._stop(this.previousTurnPoint.x, this.previousTurnPoint.y);
          otherBody._stop(otherBody.turnPoint.x, otherBody.turnPoint.y);
          otherBody.collisionHandled = true;
        } else {
          if (this._isBlocked(otherBody)) {
            this._stop(this.turnPoint.x, this.turnPoint.y);
          }
        }
      }
    }

    tank._stop = function(x, y) {
      this.state.vel.x = 0;
      this.state.vel.y = 0;
      this.moving = false;
      this.state.pos.x = x;
      this.state.pos.y = y;
      this.futureDirection = Directions.NONE;
      this.currentDirection = Directions.NONE;
    }

    tank._turn = function() {
      this.currentDirection = this.futureDirection;
      this.facing = this.futureDirection;
      this.state.pos.x = this.turnPoint.x;
      this.state.pos.y = this.turnPoint.y;

      var speed = this.TANK_SPEED;
      if (this.currentDirection === Directions.LEFT || this.currentDirection === Directions.UP) { 
        speed = -speed;
      }
      if (this.currentDirection === Directions.LEFT || this.currentDirection === Directions.RIGHT) {
        this.state.vel.x = speed; this.state.vel.y = 0;
      } else {
        this.state.vel.y = speed; this.state.vel.x = 0;
      }
      this.moving = true;
    }

    tank._calculateTurnPoint = function() {
      var tileX = Math.floor(this.state.pos.x / 64);
      var tileY = Math.floor(this.state.pos.y / 64);

      var newTurnPoint = {
        x: (tileX * 64) + 32,
        y: (tileY * 64) + 32
      }

      if (newTurnPoint.x === this.turnPoint.x && newTurnPoint.y === this.turnPoint.y) {
        return;
      }

      this.previousTurnPoint.x = this.turnPoint.x;
      this.previousTurnPoint.y = this.turnPoint.y;
      this.turnPoint.x = newTurnPoint.x;
      this.turnPoint.y = newTurnPoint.y;
    }

    tank._leftTile = function() {
      tileX = (this.turnPoint.x - 32) / 64;
      tileY = (this.turnPoint.y - 32) / 64;
      return this.world.wallsMap[(tileX - 1) + "," + tileY];
    }

    tank._rightTile = function() {
      tileX = (this.turnPoint.x - 32) / 64;
      tileY = (this.turnPoint.y - 32) / 64;
      return this.world.wallsMap[(tileX + 1) + "," + tileY];
    }

    tank._aboveTile = function() {
      tileX = (this.turnPoint.x - 32) / 64;
      tileY = (this.turnPoint.y - 32) / 64;
      return this.world.wallsMap[tileX + "," + (tileY - 1)];
    }

    tank._belowTile = function() {
      tileX = (this.turnPoint.x - 32) / 64;
      tileY = (this.turnPoint.y - 32) / 64;
      return this.world.wallsMap[tileX + "," + (tileY + 1)];
    }

    tank._isBlocked = function(otherBody) {
      if (this.facing === Directions.UP) {
        if (this.turnPoint.x === otherBody.turnPoint.x &&
                        this.turnPoint.y - 64 === otherBody.turnPoint.y) {
          return true;
        }
      } else if (this.facing === Directions.DOWN) {
        if (this.turnPoint.x === otherBody.turnPoint.x &&
                        this.turnPoint.y + 64 === otherBody.turnPoint.y) {
          return true;
        }
      } else if (this.facing === Directions.LEFT) {
        if (this.turnPoint.x - 64 === otherBody.turnPoint.x &&
                        this.turnPoint.y === otherBody.turnPoint.y) {
          return true;
        }        
      } else if (this.facing === Directions.RIGHT) {
        if (this.turnPoint.x + 64 === otherBody.turnPoint.x &&
                        this.turnPoint.y === otherBody.turnPoint.y) {
          return true;
        }        
      }
      return false;
    }

    tank._canTurn = function() {
      if (this.currentDirection === this.futureDirection) { return; }

      if (tank.futureDirection === Directions.LEFT) { var tile = this._leftTile(); }
      if (tank.futureDirection === Directions.RIGHT) { var tile = this._rightTile(); }
      if (tank.futureDirection === Directions.UP) { var tile = this._aboveTile(); }
      if (tank.futureDirection === Directions.DOWN) { var tile = this._belowTile(); }

      if (tile && tile.name === 'wall') { return; }

      // Threshold Check to ensure tank is close enough to the turnPoints
      xCheck = Math.abs(Math.floor(this.state.pos.x) - this.turnPoint.x) < 13;
      yCheck = Math.abs(Math.floor(this.state.pos.y) - this.turnPoint.y) < 13;
      if (!xCheck || !yCheck) { return; }

      return true
    }

    tank._canFire = function() {
      return Date.now() - this.lastFire > 500
    }

    tank._canFireMegaBullet = function() {
      return ((Date.now() - this.lastFireMegaBullet > 500) && this.numMegaBullets > 0)
    }

    tank._canPlaceBomb = function() {
      return ((Date.now() - this.lastBomb > 300) && this.numBombs > 0)
    }

    tank._canActivateShield = function() {
      return (!this.shielded && (Date.now() - this.lastShield) > 5000 && this.numShields > 0)
    }

    tank._activateShield = function() {
      this.shielded = true;
      this.lastShield = Date.now();
      this.numShields -= 1;
      setTimeout(function(data) {
        data[0].shielded = false;
      }, 5000, [this])
      this.world.shieldUpdates.push(this);
    }

    tank._fire = function() {
      if (this.world.room.data.info) {
        Physics.addBullet(tank.world, tank);
        this.lastFire = Date.now();
        this.world.room.data.info[this.id].bulletsShot += 1;
      }
    }

    tank._fireMegaBullet = function() {
      Physics.addMegaBullet(tank.world, tank);
      this.lastFireMegaBullet = Date.now();
      this.addMegaBullet(-1);
    }

    tank._placeBomb = function() {
      Physics.addExplosion(tank, { x: tank.turnPoint.x, y: tank.turnPoint.y }, 1000);
      this.lastBomb = Date.now();
      this.numBombs -= 1;
      this.world.bombCountUpdates.push(this);
      this.world.placedBombs.push([tank.turnPoint.x, tank.turnPoint.y])
      this.world.room.data.info[this.id].bombsUsed += 1;
    }

    world.add(tank);
    return tank
  }
}
