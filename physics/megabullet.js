module.exports = function(Physics) {
  
  Physics.addMegaBullet = function(world, tank) {
    var bullet = Physics.body('rectangle', {
      x: tank.state.pos.x,
      y: tank.state.pos.y,
      width: 28,
      height: 28
    });
    bullet.name = 'megabullet';
    bullet.tank = tank;
    bullet.world = world;

    var speed = 3.5;
    if (tank.facing === Directions.LEFT) { 
      bullet.state.vel.x = -speed;
      bullet.direction = Directions.LEFT
    }
    if (tank.facing === Directions.RIGHT) { 
      bullet.state.vel.x = speed;
      bullet.direction = Directions.RIGHT;
    }
    if (tank.facing === Directions.UP) {
      bullet.state.vel.y = -speed;
      bullet.direction = Directions.UP;
    }
    if (tank.facing === Directions.DOWN) {
      bullet.state.vel.y = speed;
      bullet.direction = Directions.DOWN;
    }
    
    bullet._collide = function(otherBody) {
      if (otherBody.name === 'wall' || (otherBody.name === 'tank' && this.tank.id !== otherBody.id)) {
        if (otherBody.name === 'wall') {
          this.tank.increaseScore(-1);
        }
        this.world.remove(this);
        this.world.megaBullets.splice(this.world.megaBullets.indexOf(this), 1);
        this.world.deadMegaBullets.push(this);
        var tank = this.tank;
        if (this.direction === Directions.UP) {
          Physics.addExplosion(tank, { x: this.state.pos.x, y: this.state.pos.y + 16 }, 0);
        }
        if (this.direction === Directions.DOWN) {
          Physics.addExplosion(tank, { x: this.state.pos.x, y: this.state.pos.y - 16 }, 0);
        }
        if (this.direction === Directions.LEFT) {
          Physics.addExplosion(tank, { x: this.state.pos.x + 16, y: this.state.pos.y }, 0);            
        }
        if (this.direction === Directions.RIGHT) {
          Physics.addExplosion(tank, { x: this.state.pos.x - 16, y: this.state.pos.y }, 0);
        }
      }
    }

    world.add(bullet);
    world.megaBullets.push(bullet);
    return bullet;
  }
}
