var Physics = require('./physicsjs-full.min');
var gameloop = require('./gameloop');

Directions = {
  NONE:   0,
  LEFT:   1,
  RIGHT:  2,
  UP:     3,
  DOWN:   4
}

getTimeLeft = function(timeout) {
  return Math.ceil((timeout._idleStart + timeout._idleTimeout - Date.now()) / 1000);
}

require('./tank')(Physics);
require('./wall')(Physics);
require('./bullet')(Physics);
require('./megabullet')(Physics);
require('./bomb')(Physics);
require('./explosion')(Physics);

module.exports = function(room) {
  var world = Physics({ sleepDisabled: true })
  world.gameOver = false;
  world.room = room;
  world.botAction = require('./bot')();

  // Create World Boundary
  var bounds = Physics.aabb(0, 0, 1600, 1600)
  bounds.name = 'bounds'

  // Setup Collision Behaviours
  world.add(Physics.behavior('body-collision-detection'));
  world.add(Physics.behavior('sweep-prune'));
  world.add(Physics.behavior('edge-collision-detection', { aabb: bounds }));

  // Create Walls
  world.walls = [];
  world.wallsMap = {};
  world.damagedWalls = [];
  var map = room.data.map;
  var fs = require('fs')
  var mapData = JSON.parse(fs.readFileSync("./public/data/" + map, 'utf8'))
  var tileData = mapData["layers"][0]["data"]
  for (var i = 0; i < 25 * 25; i++) {
    tileIndex = tileData[i];
    tileX = i % 25;
    tileY = Math.floor(i / 25);

    if (tileIndex === 2) {
      var wall = Physics.addWall(world, tileX, tileY);
      world.walls.push(wall);
      world.wallsMap[tileX + "," + tileY] = wall; 
    }
  }

  // Create Tanks
  var GRID_SIZE = 64;
  var x = GRID_SIZE;
  var startingPositions = [ [2 * x, 1 * x], [12 * x, 1 * x], [22 * x, 1 * x], 
                            [1 * x, 7 * x], [23 * x, 7 * x], [1 * x, 17 * x],
                            [23 * x, 17 * x], [2 * x, 23 * x], [12 * x, 23 * x], 
                            [22 * x, 23 * x] ]
  var i = 0;
  world.tanks = [];
  world.tanksMap = {};
  world.aliveTanks = [];
  world.deadTankUpdates = [];
  world.notifyTimeOut = [];
  world.notifyWonGame = [];
  world.shieldUpdates = [];
  for (var id in room.data.info) {
    var x = startingPositions[i][0];
    var y = startingPositions[i][1];
    var tank = Physics.addTank(world, x, y, id, room.data.info[id].isBot);
    world.tanks.push(tank);
    world.aliveTanks.push(tank);
    world.tanksMap[id] = tank;
    i++;
  }

  // Set Up Bullets
  world.bullets = [];
  world.deadBullets = [];

  // Set Up Megabullets
  world.megaBullets = [];
  world.deadMegaBullets = [];
  world.megaBulletCountUpdates = [];

  // Create Bombs
  world.bombsMap = {};
  world.pickedUpBombs = [];
  world.placedBombs = [];
  world.bombCountUpdates = [];
  for (var i = 0; i < room.data.bombPositions.length; i++) {
    var x = room.data.bombPositions[i][0];
    var y = room.data.bombPositions[i][1];
    var bomb = Physics.addBomb(world, x, y);
    world.bombsMap[bomb.uid] = bomb;
  }

  // Set Up Score / Health / Streak Updates
  world.scoreUpdates = [];
  world.healthUpdates = [];
  world.streakUpdates = [];
  world.actionUpdates = [];
  world.noticeUpdates = [];

  // Create Living Reward Timer
  world.livingRewardTimer = setInterval(function(data) {
    var world = data[0];
    if (world.gameOver) { return; }
    for (var i = 0; i < world.tanks.length; i++) {
      var tank = world.tanks[i];
      if (tank.isAlive()) { 
        tank.increaseScore(1);
      }
    }
  }, 10000, [world])

  // Collision Handler
  world.on('collisions:detected', function(data, event) {
    for (var i = 0, l = data.collisions.length; i < l; i++) {
      var c = data.collisions[i];
      if (c.bodyA._collide) { c.bodyA._collide(c.bodyB); }
      if (c.bodyB._collide) { c.bodyB._collide(c.bodyA); }
    }
  });

  // Game Over Checker
  world.checkGameOver = function() {
    if (this.aliveTanks.length <= 1) {
      console.log("We have a winner!");
      world.gameOver = true;
      world.pause();
      clearInterval(world.livingRewardTimer);
      clearTimeout(world.gameTimeout);
      this.notifyWonGame = this.aliveTanks.slice(0);
      this.notifyWonGame[0].increaseScore(5);
    }
  }

  // Main
  world.refresh = function() {
    if (world.gameOver) { return; }
    world.checkGameOver();
    var tanks = this.tanks;
    for (var i = 0; i < tanks.length; i++) {
      var tank = tanks[i];
      if (tank.isAlive()) { 
        if (tank.isBot) {
          world.botAction(tank, world);
        }
        tank.refresh();
      }
    }
  }
  
  world.timeRemaining = function() {
    return Math.round(((this.startTime + this.timeAllowed) - Date.now()) / 1000);
  }

  world.update = function(delta) {
    this.step();
    this.refresh();
  }

  world.start = function() {
    if (this.loopActive) { return; }
    console.log("Game Started...")
    this.loopInterval = gameloop.setGameLoop(world, 1000 / 60);
    this.loopActive = true;

    // Start Game Timer
    this.timeAllowed = 2000 * 60 * 1
    this.gameTimeout = setTimeout(function(data) {

      console.log(data[0].room.name);
      console.log("Timer Ended. GAME OVER!");
      data[0].gameOver = true;
      data[0].pause();
      clearInterval(data[0].livingRewardTimer);
      data[0].notifyTimeOut = data[0].tanks.slice(0);

    }, this.timeAllowed, [this])
    this.startTime = Date.now();

    var data = new Buffer(2);
    data.writeUInt16BE(this.timeRemaining(), 0);
    this.room.messageMembers('timeUpdate', data);
  }

  world.stop = function() {
    gameloop.clearGameLoop(this.loopInterval);
    this.loopActive = false;
  }

  return world;
}
