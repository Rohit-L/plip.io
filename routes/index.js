var express = require('express');
var router = express.Router();

var isAuthenticated = function(req, res, next) {
  if (req.query.guest == "true") {
    return next();
  }
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/?guest=true');
}

module.exports = function(passport) {

  /* GET root page. */
  router.get('/', isAuthenticated, function(req, res) {
    var user = req.user;
    var id = user ? user.fb.id : null
    res.render('index', { id: id, title: ' plip.io' });
  });

  router.get('/guest', function(req, res) {
    res.redirect('/');
  });

  /* Handle Logout */
  router.get('/signout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

  // route for facebook authentication and login
  // different scopes while logging in
  router.get('/login/facebook', 
    passport.authenticate('facebook', { scope: 'email'})
  );

  // handle the callback after facebook has authenticated the user
  router.get('/login/facebook/callback',
    passport.authenticate('facebook', {
      successRedirect : '/',
      failureRedirect : '/login'
    })
  );

  return router;
}
