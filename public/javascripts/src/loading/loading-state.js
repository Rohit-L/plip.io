window.LoadingState = function() {
  return {
    create: function() {

      window.loading_screen = window.pleaseWait({
        backgroundColor: '#489748',
        loadingHtml: '<div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>'
      });
      setTimeout(function(data) {
        window.loading_screen.finish();
        data[0].game.state.start('GameState');
      }, 3000, [this])
      
    }
  }
}
