$(function() {
  // Opera 8.0+
  var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  // Firefox 1.0+
  var isFirefox = typeof InstallTrigger !== 'undefined';
  // At least Safari 3+: "[object HTMLElementConstructor]"
  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
  // Internet Explorer 6-11
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  // Edge 20+
  var isEdge = !isIE && !!window.StyleMedia;
  // Chrome 1+
  var isChrome = !!window.chrome && !!window.chrome.webstore;
  // Blink engine detection
  var isBlink = (isChrome || isOpera) && !!window.CSS;

  if (isSafari || isEdge || isIE || isFirefox) {
    $('#googleAd').css({'display': 'none'})
    return
  }

  var gameConfiguration = {
    'width': 1920,
    'height': 1080,
    'renderer': Phaser.AUTO,
    'parent': 'game',
    'transparent': true,
    'antialias': false,
    "resolution": 1
  };
  Game = new Phaser.Game(gameConfiguration);
  network.configure({
    serverEvents: {
      begin: function() {
        var id = $('#fbid').text();
        var idBuffer = new TextEncoder("utf-8").encode(id);
        var data = new Uint8Array(idBuffer.length + 1);
        data.set(idBuffer, 1);
        network.message('connect', new DataView(data.buffer));

        //Game.state.add('LobbyState', window.LobbyState, true);
        Game.state.add('GameState', window.GameState, false);
        Game.state.add('LobbyState', window.LobbyState, false);
        Game.state.add('LoadingState', window.LoadingState, false);
        Game.data = {};
        Game.data.stats = {};
        Game.state.start('LobbyState');
      },

      joinedRoom: function(roomArrayBuffer) { // You joined room
        var room = new TextDecoder("utf-8").decode(new Uint8Array(roomArrayBuffer));

        if (room === 'Lobby') {
          console.log("JOINED LOBBY")
          Game.data.inLobby = true;

        } else { // Joining a Game room
          console.log("JOINED GAME ROOM")
          Game.data.state = {};

          var nickname = $('#input').val();
          if (!nickname) { nickname = "Anon"; }

          var nicknameBuffer = new TextEncoder("utf-8").encode(nickname);
          var data = new Uint8Array(nicknameBuffer.length + 1);
          data.set(nicknameBuffer, 1);
          network.message('requestIDs', new DataView(data.buffer));

        }
      },

      leftRoom: function(roomBuffer) { // You left room
        var room = new TextDecoder("utf-8").decode(new Uint8Array(roomBuffer))

        // Leaving room but not joining lobby -- should return to main screen
        if (room !== 'Lobby') {
          console.log("LEFT GAME ROOM")
          Game.data.inLobby = false;
          delete Game.data.state;
          Game.state.start('LobbyState');
          $('#lobby').removeClass('hide');
        }
      },

      roomMemberLeft: function(data) {
        var id = new TextDecoder("utf-8").decode(new Uint8Array(data));
        if (Game.state.getCurrentState() && Game.state.getCurrentState().allTanks) {
          Game.state.getCurrentState().removeTank(id);
        }
        if (id !== network.currentUser()) {
          delete Game.data.state.playerIDs[Game.data.state.playerIDs.indexOf(id)];
        }
      }
    }, // End Server Events

    messages: {

      tankUpdate: function(data) {
        var id = new TextDecoder("utf-8").decode(new Uint8Array(data.slice(0, 36)));
        var dataView = new DataView(data.slice(36));
        var x = dataView.getUint16(0);
        var y = dataView.getUint16(2);
        var currentDirection = dataView.getUint8(4);
        var moving = dataView.getUint8(5) === 1 ? true : false;
        var state = Game.state.getCurrentState();
        if (state && state.allTanks && state.allTanks[id]) {
          state.allTanks[id].refresh(x, y, currentDirection, moving);
        }
      },

      bulletUpdate: function(data) {
        if (!Game.data.state.playing) { return; }

        var dataView = new DataView(data);
        var x = dataView.getUint16(0);
        var y = dataView.getUint16(2);
        var id = dataView.getUint16(4);
        var direction = dataView.getUint8(6);
        var state = Game.state.getCurrentState();
        
        if (Game.state.getCurrentState().activeBullets[id]) {
          var bullet = state.activeBullets[id];
          bullet.refresh(x, y, direction);
        } else {
          var bullet = state.deadBullets.splice(0, 1)[0];
          state.activeBullets[id] = bullet;
          bullet.id = id;
          Game.state.getCurrentState().sounds.bulletFire();
          bullet.refresh(x, y, direction, true);
        }
      },

      killBullet: function(data) {
        if (!Game.data.state.playing) { return; }

        var state = Game.state.getCurrentState();
        var id = (new DataView(data)).getUint16(0);
        var bullet = state.activeBullets[id];
        if (bullet) {
          bullet.collided();
          state.deadBullets.push(bullet);
        }
        delete state.activeBullets[id];
      },

      megaBulletUpdate: function(data) {
        var dataView = new DataView(data);
        var x = dataView.getUint16(0);
        var y = dataView.getUint16(2);
        var id = dataView.getUint16(4);
        var direction = dataView.getUint8(6);
        var state = Game.state.getCurrentState();
        
        if (Game.state.getCurrentState().activeMegaBullets[id]) {
          var megaBullet = state.activeMegaBullets[id];
          megaBullet.refresh(x, y, direction);
        } else {
          var megaBullet = new Bullet(Game, x, y, 'megaBullet');
          state.activeMegaBullets[id] = megaBullet;
          megaBullet.id = id;
          Game.state.getCurrentState().sounds.bulletFire();
          megaBullet.refresh(x, y, direction);
        }
      },

      killMegaBullet: function(data) {
        var state = Game.state.getCurrentState();
        var id = (new DataView(data)).getUint16(0);
        var megaBullet = state.activeMegaBullets[id];
        megaBullet.collided();
        delete state.activeMegaBullets[id];
      },

      bombPositions: function(data) {
        var data = (new TextDecoder("utf-8").decode(new Uint8Array(data))).split("<|>");
        data.pop();
        Game.data.state.bombPositions = []
        for (var i = 0; i < data.length; i++) {
          var info = data[i].split("|");
          Game.data.state.bombPositions.push({ x: info[0], y: info[1], id: info[2] });
        }
      },

      pickedUpBomb: function(data) {
        if (!Game.data.state.playing) { return; }

        var id = (new DataView(data)).getUint16(0);
        var bomb = Game.state.getCurrentState().bombsMap[id];
        bomb.pickedUp();
        delete Game.state.getCurrentState().bombsMap[id];
        Game.state.getCurrentState().pickedUpBombs.push(bomb);
      },

      placedBomb: function(data) {
        var dataView = new DataView(data);
        var bomb = Game.state.getCurrentState().pickedUpBombs.pop();
        bomb.activate({ 
          x: dataView.getUint16(0),
          y: dataView.getUint16(2)
        });
      },

      damageWall: function(data) {
        if (!Game.data.state.playing) { return; }

        var dataView = new DataView(data);
        var data = {
          tileX: dataView.getUint16(0),
          tileY: dataView.getUint16(2),
          health: dataView.getUint16(4)
        }
        Game.state.getCurrentState().map.crackTile(data);
      },

      scoreUpdate: function(data) {
        var id = new TextDecoder("utf-8").decode(new Uint8Array(data.slice(0, 36)));
        var dataView = new DataView(data.slice(36));
        Game.data.state[id].score = dataView.getUint16(0);
      },

      healthUpdate: function(data) {
        if (!Game.data.state.playing) { return; }

        var id = new TextDecoder("utf-8").decode(new Uint8Array(data.slice(0, 36)));
        var dataView = new DataView(data.slice(36));
        Game.data.state[id].health = dataView.getUint8(0);
        if (Game.data.state[id].health === 0) {
          Game.state.getCurrentState().removeTank(id);
        } else {
          Game.state.getCurrentState().allTanks[id].refreshHealthBar();
        }
      },

      deadTankUpdate: function(data) { // You died
        if (!Game.data.state || !Game.data.state.playing) { return ;}

        Game.state.getCurrentState().isAlive = false;
        var lastAssaulterID = new TextDecoder("utf-8").decode(new Uint8Array(data));
        Game.state.getCurrentState().myTank.lastAssaulterID = lastAssaulterID;
      },

      bombCountUpdate: function(data) {
        var id = new TextDecoder("utf-8").decode(new Uint8Array(data.slice(0, 36)));
        var dataView = new DataView(data.slice(36));
        Game.data.state[id].numBombs = dataView.getUint8(0);
      },

      megaBulletCountUpdate: function(data) {
        var id = new TextDecoder("utf-8").decode(new Uint8Array(data.slice(0, 36)));
        var dataView = new DataView(data.slice(36));
        Game.data.state[id].numMegaBullets = dataView.getUint8(0);
      },

      timeOut: function() {
        Game.state.getCurrentState().timeOut = true;
      },

      wonGame: function() {
        Game.state.getCurrentState().wonGame = true;
      },

      actionUpdate: function(data) {
        if (!Game.data.state.playing) { return; }

        var update = JSON.parse(new TextDecoder("utf-8").decode(new Uint8Array(data)))
        Game.state.getCurrentState().actionFeed.appendAction(update);
      },

      streakUpdate: function(data) {
        var dataView = new DataView(data);
        var streak = dataView.getUint16(0);
        var state = Game.state.getCurrentState();
        Game.data.state[network.currentUser()].streak = streak;
        if (streak > 0 && streak % 5 === 0) {
          if (streak % 10 === 0) {
            state.actionFeed.appendNotice([state.myTank.id, 'bonus', [streak]]);
          } else {
            state.actionFeed.appendNotice([state.myTank.id, 'streak', [streak]]);
          }
        }
      },

      timeUpdate: function(data) {
        var dataView = new DataView(data);
        var timeRemaining = dataView.getUint16(0);
        Game.data.state.time = { remaining: timeRemaining, set: Date.now(), playing: true };
      },

      preparingRoom: function() {
        Game.data.state.time = { remaining: 4, set: Date.now(), playing: false };
      },

      shieldActivated: function(data) {
        var id = new TextDecoder("utf-8").decode(new Uint8Array(data.slice(0, 36)));
        var numShields = (new DataView(data.slice(36))).getUint8(0);
        Game.data.state[id].numShields = numShields;
        Game.state.getCurrentState().allTanks[id].activateShield();
      },

      startGame: function(data) {
        
        // Creating map from id to nicknames
        var data = new TextDecoder("utf-8").decode(new Uint8Array(data));
        data = data.split("<|>");
        data.pop();
        var nicknames = {};
        for (var i = 0; i < data.length; i++) {
          var userString = data[i];
          nicknames[userString.substr(0, 36)] = userString.substr(36);
        }

        var leaderboard = [];
        var i = 1;
        for (var id in nicknames) {
          var nickname = nicknames[id];
          if (nickname === 'Anon') {
            nicknames[id] = "Anon " + i;
            i++;
          }
          leaderboard.push(nicknames[id]);
        }

        Game.data.state.leaderboard= leaderboard;
        Game.data.state.nicknames = nicknames;

        $('#lobby').addClass('hide');
        if (Game.state.getCurrentState()) {
          Game.state.getCurrentState().dontUpdate = false;
        }
        Game.state.start('LoadingState');
      },

      responseIDs: function(idsArrayBuffer) {
        var idString = new TextDecoder("utf-8").decode(new Uint8Array(idsArrayBuffer));
        
        var ids = [];
        while (idString.length > 1) {
          var id = idString.substr(0, 36);
          Game.data.state[id] = { score: 0, health: 20, numBombs: 0, 
                                  numMegaBullets: 5, streak: 0, numShields: 2 };
          ids.push(id);
          idString = idString.substr(36);
        }

        Game.data.state.playerIDs = ids;
        network.message('readyToPlay');
      },
      
      gameStats: function(data) {
        var dataView = new DataView(data);
        Game.data.stats.game = {}
        Game.data.stats.game.highestScore = dataView.getUint16(0);
        Game.data.stats.game.totalGamesPlayed = dataView.getUint16(2);
        Game.data.stats.game.minRoomMembers = dataView.getUint16(4);
        Game.data.stats.game.lobbyLength = dataView.getUint16(6);
        Game.data.stats.game.roomCount = dataView.getUint16(8);
        Game.data.stats.game.playerCount = dataView.getUint16(10);
      },
      
      statistics: function(myStatsArrayBuffer) {
        var dataView, myStats;
        var dataView = new DataView(myStatsArrayBuffer);
        var personalStats = {
          'gamesPlayed': dataView.getUint16(0),
          'totalScore': dataView.getUint16(2),
          'gamesWon': dataView.getUint16(4),
          'hitsTaken': dataView.getUint16(6),
          'bulletsShot': dataView.getUint16(8),
          'bulletsHitOtherPlayer': dataView.getUint16(10),
          'bombsUsed': dataView.getUint16(12),
          'highScore': dataView.getUint16(14)
        };
        
        Game.data.stats.personal = personalStats;
      },

      mapSelection: function(data) {
        var map = new TextDecoder("utf-8").decode(new Uint8Array(data));
        Game.data.state.map = map;
      }
    } // End Custom Messages
  }); // End Network Configuration

  window.onbeforeunload = function() {
    network._disconnect();
  };

  network.run(window.location.href);

  window.sendInput = function(input) {
    var state = Game.state.getCurrentState();
    if (!state.isAlive || state.gameOver || state.timeOut) { return; }

    var inputCodes = {
      "spacebarDown": 0,
      "spacebarUp": 1,
      "bKeyDown": 2,
      "bKeyUp": 3,
      "vKeyDown": 4,
      "vKeyUp": 5,
      "upKeyDown": 6,
      "downKeyDown": 7,
      "leftKeyDown": 8,
      "rightKeyDown": 9,
      "cKeyDown": 10,
      "cKeyUp": 11
    }
    
    var key = inputCodes[input];
    var data = new DataView(new ArrayBuffer(2));
    data.setUint8(1, key);
    network.message('input', data);
  }

});
