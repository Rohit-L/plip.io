/**
* @file lobby-state.js
* Class definition for the lobby state. -- THIS IS A NECESSARY DUMMY STATE
*
* @project plip.io
* @author
*   Pranay Kumar
*   Rohit Lalchandani
*/
window.LobbyState = function() {
  return {
    preload: function() {
      this.game.scale.pageAlignHorizontally = true;
      this.game.scale.pageAlignVertically = true;
      this.game.stage.smoothed = false;
      this.game.stage.disableVisibilityChange = true;
      
      this.load.spritesheet('blueTank', 'images/blueTank.png', 128, 128);
      this.load.spritesheet('greenTank', 'images/greenTank.png', 128, 128);
      this.load.spritesheet('blackTank', 'images/blackTank.png', 128, 128);
      this.load.spritesheet('beigeTank', 'images/beigeTank.png', 128, 128);
      this.load.spritesheet('orangeTank', 'images/orangeTank.png', 128, 128);
      this.load.spritesheet('explosion', 'images/explosion.png', 128, 128);
      this.load.spritesheet('bomb', 'images/bomb-sprites.png', 51, 51);
      this.load.spritesheet('crumble', 'images/crumble.png', 512, 512)

      this.load.image('bullet', 'images/bulletBlue.png');
      this.load.image('megaBullet', 'images/megaBullet.png');
      this.load.image('filledSuperBullet', 'images/filledSuperBullet.png');
      this.load.image('exitButton', 'images/exitButton.png');
      this.load.image('spectateButton', 'images/spectateButton.png');
      this.load.image('bar', 'images/bar.png');
      this.load.image('shield', 'images/shield.png')

      this.load.audio('bulletFired', 'sounds/bulletFired.ogg');
      this.load.audio('bombExplosion', 'sounds/bomb.ogg');
      this.load.audio('beep', 'sounds/beep.ogg');
      this.load.audio('bulletHit', 'sounds/bulletHit.ogg');
      this.load.audio('music', 'sounds/music.ogg');
      this.load.audio('crack', 'sounds/crack.ogg');
    },
  }
}
