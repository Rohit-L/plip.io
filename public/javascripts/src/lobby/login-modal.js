$(function() {
  // Opera 8.0+
  var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  // Firefox 1.0+
  var isFirefox = typeof InstallTrigger !== 'undefined';
  // At least Safari 3+: "[object HTMLElementConstructor]"
  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
  // Internet Explorer 6-11
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  // Edge 20+
  var isEdge = !isIE && !!window.StyleMedia;
  // Chrome 1+
  var isChrome = !!window.chrome && !!window.chrome.webstore;
  // Blink engine detection
  var isBlink = (isChrome || isOpera) && !!window.CSS;

  if (isSafari || isEdge || isIE || isFirefox) {
    swal({
      title: "Oh, no!",
      text: "Your browser is not supported.<br><br>Try using <a href='https://www.google.com/chrome/browser/desktop/'>Chrome</a> for the best experience!",
      html: true,
      showConfirmButton: false,
      type: 'error'
    })
    return
  }
})
