$(function() {

  window.gameSettings = { highResolution: true, music: true, sounds: true }

  var resize = function(amount=0.2) {
    $('#lobby-title').css({ "marginTop": (amount * window.innerHeight) + "px" })
  }
  resize()

  var id;
  var resizer = function(gameState) {
    $(window).resize(function() {
      clearTimeout(id);
      var amount = window.innerHeight <= 482 ? 0.1 : 0.2;
      id = setTimeout(function() {
        resize(amount)
      }, 5);
    });
  }
  resizer(this)

  setInterval(function() {
    $('#lobby-subtitle').fadeOut(2000, function() {
      var phrases = [
        'Defeat the Other Tanks',
        'Use Your Shield Wisely',
        'MegaBullets & Bombs Deal Double Damage',
        'The Minimap Shows Other Players',
        'Recieve a Bonus for Every 10 Hits',
        'Missing a MegaBullet Decreases Score',
        'The Last Tank Alive Gets a Bonus'
      ]
      $(this).text(phrases[Math.floor(Math.random() * phrases.length)]).fadeIn(2000);
    })
  }, 2000)

  $('#joinGameButton').on('click', function() {
    if (Game.data.inLobby) { return; }
    network.message('readyButtonPressed');
  });

  $('#settings-icon').on('click', function() {
    $('#settings-panel').removeClass('hide');
    if ($('#settings-panel').hasClass('fadeOutRight')) {
      $('#settings-panel').removeClass('fadeOutRight');
    }
    $('#settings-panel').addClass('fadeInRight');
  });

  $('#settings-save-button').on('click', function() {
    $('#settings-panel').removeClass('fadeInRight');
    $('#settings-panel').addClass('fadeOutRight');
    setTimeout(function() {
      $('#settings-panel').addClass('hide');
    }, 700)
  });

  $('#music-setting').on('change', function() {
    window.gameSettings.music = $(this).is(':checked');
  })

  $('#sound-setting').on('change', function() {
    window.gameSettings.sounds = $(this).is(':checked');
  })

  $('#resolution-setting').on('change', function() {
    oldData = Game.data
    oldResolution = Game.resolution
    Game.destroy();
    var config = {
      'width': 1920,
      'height': 1080,
      'renderer': Phaser.CANVAS,
      'parent': 'game',
      'transparent': true,
      'antialias': true,
      "resolution": oldResolution === 1 ? 0.7 : 1
    };
    Game = new Phaser.Game(config);
    Game.data = oldData;
    Game.state.add('GameState', window.GameState, false);
    Game.state.add('LobbyState', window.LobbyState, false);
    Game.state.add('LoadingState', window.LoadingState, false);
    Game.state.start('LobbyState');
  })

  setInterval(function() {
    if (Game.data) {
      if (Game.data.stats) {

        if (!Game.data.stats.personal) {
          $('#personal-statistics-table').hide();
          $('#personal-statistics-message').show();
        } else {
          var personalStatistics = Game.data.stats.personal
          $('#personal-statistics-table').show();
          $('#personal-statistics-message').hide();
          $('#games-played').text(personalStatistics.gamesPlayed);
          $('#average-score').text((personalStatistics.totalScore / personalStatistics.gamesPlayed).toFixed(2));
          $('#games-won').text(personalStatistics.gamesWon);
          $('#damage-ratio').text((personalStatistics.bulletsHitOtherPlayer / personalStatistics.hitsTaken).toFixed(2));
          $('#shot-accuracy').text((personalStatistics.bulletsHitOtherPlayer / personalStatistics.bulletsShot).toFixed(2));
          $('#bombs-used').text(personalStatistics.bombsUsed);
          $('#high-score').text(personalStatistics.highScore);
        }

        if (Game.data.stats.game) {
          $('#total-games').text(Game.data.stats.game.totalGamesPlayed);
          $('#highest-score').text(Game.data.stats.game.highestScore);
          $('#active-games-value').text(Game.data.stats.game.roomCount);
          $('#active-players-value').text(Game.data.stats.game.playerCount);
        }
      }

      if (Game.data.inLobby) {
        var players = Game.data.stats.game.minRoomMembers - Game.data.stats.game.lobbyLength;
        if (players === 1) {
          $('#joinGameButton').text("Waiting for " + players + " more player");
        } else {
          $('#joinGameButton').text("Waiting for " + players + " more players");
        }
      } else {
        $('#joinGameButton').text("Join Lobby");
      }
    }

  }, 250)

})
