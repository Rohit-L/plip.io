/**
 * @file scoreboardTank.js
 * Class definition for the scoreboard's tank.
 *
 * @project plip.io
 * @author
 *   Pranay Kumar
 *   Rohit Lalchandani
 */
function ScoreboardTank(scoreboard, tank, initialX, initialY) {
  this.scoreboard = scoreboard;
  this.state = scoreboard.state;
  this.game = scoreboard.game;
  this.tank = tank;
  this.initialX = initialX;
  this.initialY = initialY;
  this.scoreOffset = 13;
  this.totalWidth = (this.scoreboard.INFO_BOX_WIDTH - (3 * this.scoreboard.PADDING));

  this.init = function() {
    this.NAME_STYLE = {backgroundColor: 'rgba(255, 255, 255, .5)', font: '12pt Orbitron', align: 'center'};
    this.MY_NAME_STYLE = {backgroundColor: 'rgba(255, 255, 255, .5)', font: 'bold 13pt Orbitron', align: 'center'};
    this.NUMBER_STYLE = {font: '12pt Orbitron', align: 'center', fill: 'white'};
    this._createName();
    this._createTank();
    this._createTankScore();
    this._createTankBar();

    return this;
  };

  this.kill = function() {
    this._name.kill();
    this._tank.kill();
    this._tankScore.kill();
    this._tankBar.kill();
  };

  this.toggleVisibility = function(alpha) {
    this._name.alpha = alpha;
    this._tank.alpha = alpha;
    if (this._tankScore) {
      this._tankScore.alpha = alpha;
    }
    this._tankBar.alpha = alpha;
  };

  this._createName = function() {
    var style = this.tank === this.state.myTank ? this.MY_NAME_STYLE : this.NAME_STYLE
    this._name = this.game.add.text(0, 0, ' ' + this.game.data.state['nicknames'][this.tank.id] + ' ', style);
    this._name.fixedToCamera = true;
    this._name.cameraOffset.setTo(this.initialX - 20, this.initialY);
    this._name.anchor.setTo(1, 0);
  };

  this._createTank = function() {
    this._tank = this.game.add.image(0, 0, this.tank.type);
    this._tank.fixedToCamera = true;
    this._tank.cameraOffset.setTo(this.initialX, this.initialY);
    this._tank.angle = 90;
    this._tank.width = this.scoreboard.TANK_DIM;
    this._tank.height = this.scoreboard.TANK_DIM;
    this._tank.anchor.setTo(0, 1);
  };

  this._createTankBar = function() {
    this._tankBar = this.game.add.graphics(0, 0);
    this._tankBar.fixedToCamera = true;
    this._tankBar.cameraOffset.setTo(this._tank.cameraOffset.x, this._tank.cameraOffset.y);
    this._tankBar.cameraOffset.y += this._tank.width * 0.5;
    this._tankBar.beginFill(0x00FF00, 1);
    this._tankBar.drawRect(0, 0, 0, 4);
    this._tankBar.endFill();
  };

  this._createTankScore = function() {
    if (this.game.data.state[this.tank.id]) {
      this._tankScore = this.game.add.text(0, 0, this.game.data.state[this.tank.id].score, this.NUMBER_STYLE);
      this._tankScore.fixedToCamera = true;
      this._tankScore.cameraOffset.setTo(this._tank.cameraOffset.x + this.scoreOffset, this._tank.cameraOffset.y + this.scoreOffset);
      this._tankScore.anchor.setTo(0.5, 0.5);
    }
  };

  this._repositionName = function() {
    this._name.setText(' ' + this.game.data.state['nicknames'][this.tank.id] + ' ');
    this._name.setStyle(this.tank === this.state.myTank ? this.MY_NAME_STYLE : this.NAME_STYLE);
    this._name.cameraOffset.setTo(this.initialX - 20, this.initialY);
  };

  this.refresh = function(maxScore) {
    if (maxScore > 0 && this.game.data.state[this.tank.id].score <= maxScore && !this.state.overlays.active) {
      this._tank.kill();
      this._tankScore.kill();
      this._repositionName();
      this._createTank();
      this._tank.cameraOffset.x = this.initialX + this.totalWidth * (this.game.data.state[this.tank.id].score / maxScore);
      this._createTankScore();
      this._tankBar.clear();
      this._tankBar.beginFill(0x00FF00, 1);
      this._tankBar.drawRect(0, 0, this._tank.cameraOffset.x - this._tankBar.cameraOffset.x, 4);
      this._tankBar.endFill();
    }
  }

}
