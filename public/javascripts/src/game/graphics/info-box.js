/**
 * @file info-box.js
 * Class definition for the info box in the game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function InfoBox(gameState) {
  this.state = gameState;
  this.game = gameState.game;

  this.init = function() {
    // Main Constants
    this.INFO_BOX_HEIGHT = 150;
    this.INFO_BOX_WIDTH = 75;
    this.INFO_BOX_STYLE = { font: "12pt Orbitron", align: 'center', fill: 'white' };
    this.INFO_BOX_PADDING = 10;
    this.BULLET_IMAGE_WIDTH = 37;
    this.BULLET_IMAGE_HEIGHT = 33;
    this.SHIELD_IMAGE_WIDTH = 36
    this.SHIELD_IMAGE_HEIGHT = 36
    this.NUM_ITEMS = 3

    this._box = this.game.add.graphics(0, 0);
    this._box.fixedToCamera = true;
    this._drawBox()

    this._bombImage = this.game.add.image(0, 0, 'bomb');
    this._bombImage.fixedToCamera = true;
    this._bombImage.anchor.setTo(0.5, 0.5);
    this._bombText = this.game.add.text(0, 0, '', this.INFO_BOX_STYLE);
    this._bombText.fixedToCamera = true;
    this._bombText.anchor.setTo(0.5, 0.5);
    this._positionBomb()

    this._superBulletImage = this.game.add.image(0, 0, 'filledSuperBullet');
    this._superBulletImage.width = this.BULLET_IMAGE_WIDTH;
    this._superBulletImage.height = this.BULLET_IMAGE_HEIGHT;
    this._superBulletImage.fixedToCamera = true;
    this._superBulletImage.anchor.setTo(0.5, 0.5);
    this._superBulletText = this.game.add.text(0, 0, '', this.INFO_BOX_STYLE);
    this._superBulletText.fixedToCamera = true;
    this._superBulletText.anchor.setTo(0.5, 0.5);
    this._positionBullet()

    this._shieldImage = this.game.add.image(0, 0, 'shield')
    this._shieldImage.width = this.SHIELD_IMAGE_WIDTH
    this._shieldImage.height = this.SHIELD_IMAGE_HEIGHT
    this._shieldImage.fixedToCamera = true
    this._shieldImage.anchor.setTo(0.5, 0.5)
    this._shieldText = this.game.add.text(0, 0, '', this.INFO_BOX_STYLE)
    this._shieldText.fixedToCamera = true
    this._shieldText.anchor.setTo(0.5, 0.5)
    this._positionShield()

    return this
  }

  this.refresh = function() {
    this._superBulletText.setText(this.game.data.state[this.state.myTank.id].numMegaBullets)
    this._bombText.setText(this.game.data.state[this.state.myTank.id].numBombs)
    this._shieldText.setText(this.game.data.state[this.state.myTank.id].numShields)
  }

  this.reposition = function() {
    this._box.clear()
    this._drawBox()
    this._positionBomb()
    this._positionBullet()
    this._positionShield()
  }

  this._drawBox = function() {
    this._box.cameraOffset.setTo(0, this.state.GAME_HEIGHT - this.INFO_BOX_HEIGHT);
    this._box.lineStyle(this.state.MENU_BORDER_WIDTH, this.state.MENU_BORDER_COLOR);
    this._box.beginFill(this.state.MENU_FILL_COLOR, this.state.MENU_FILL_ALPHA);
    this._box.drawRect(0, 0, this.INFO_BOX_WIDTH, this.INFO_BOX_HEIGHT);
    this._box.endFill();
  }

  this._positionBomb = function() {
    this._bombImage.cameraOffset.setTo(
      this.INFO_BOX_WIDTH * 0.5,
      this.state.GAME_HEIGHT - this.INFO_BOX_HEIGHT - this.INFO_BOX_PADDING + 
                      (2 * ((this.INFO_BOX_HEIGHT - (2 * this.INFO_BOX_PADDING)) / this.NUM_ITEMS))
    )
    this._bombText.cameraOffset.setTo(this._bombImage.cameraOffset.x - 3, this._bombImage.cameraOffset.y + 7)
  }

  this._positionBullet = function() {
    this._superBulletImage.cameraOffset.setTo(
      this.INFO_BOX_WIDTH * 0.5,
      this.state.GAME_HEIGHT - this.INFO_BOX_HEIGHT - this.INFO_BOX_PADDING +  
                      (1 * ((this.INFO_BOX_HEIGHT - (2 * this.INFO_BOX_PADDING)) / this.NUM_ITEMS))
    );
    this._superBulletText.cameraOffset = this._superBulletImage.cameraOffset
    this._superBulletText.cameraOffset.x -= 1
  }

  this._positionShield = function() {
    this._shieldImage.cameraOffset.setTo(
      this.INFO_BOX_WIDTH * 0.5,
      this.state.GAME_HEIGHT - this.INFO_BOX_HEIGHT - this.INFO_BOX_PADDING +  
                      (3 * ((this.INFO_BOX_HEIGHT - (2 * this.INFO_BOX_PADDING)) / this.NUM_ITEMS) + 7)
    );
    this._shieldText.cameraOffset = this._shieldImage.cameraOffset
    this._shieldText.cameraOffset.x -= 1    
  }
}
