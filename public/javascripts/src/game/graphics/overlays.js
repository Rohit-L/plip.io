/**
 * @file overlays.js
 * Class definition for the various overlays in the game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function Overlays(gameState) {
  this.state = gameState
  this.game = gameState.game

  this.init = function() {
    this.active = false
    this.FONT = 'Orbitron'

    this._mask = this.game.add.graphics(0, 0)
    this._mask.fixedToCamera = true
    this._mask.cameraOffset.setTo(0, 0)
    this._drawMask()
    this._hideMask()

    this._exitButton = this.game.add.button(0, 0, 'exitButton',
      this.state.shutdown,
      this.state
    )
    this._exitButton.anchor.setTo(0.5, 0.5)
    this._exitButton.fixedToCamera = true
    this._positionExitButton()
    this._hideExitButton()

    this._leaderboardText = this.game.add.text(0, 0, "Calculating Scores...", {
      font: '18pt ' + this.FONT,
      fill: '#FFFFFF',
      align: 'center'
    })
    this._leaderboardText.anchor.setTo(0.5, 0.5)
    this._leaderboardText.fixedToCamera = true
    this._positionLeaderboard()
    this._leaderboardText.visible = false

    this._titleText = this.game.add.text(0, 0, '', {
      font: '30pt ' + this.FONT,
      fill: '#FFFFFF',
      align: 'center'
    });
    this._titleText.anchor.set(0.5, 0.5);
    this._titleText.fixedToCamera = true;
    this._positionTitleText()
    this._hideTitleText()


    this._subtitleText = this.game.add.text(0, 0, '', {
      font: '20pt ' + this.FONT,
      fill: '#FFFFFF',
      align: 'center'
    });
    this._subtitleText.anchor.set(0.5, 0.5)
    this._subtitleText.fixedToCamera = true
    this._subtitleText2 = this.game.add.text(0, 0, '', {
      font: '20pt ' + this.FONT,
      fill: '#FFFFFF',
      align: 'center'
    });
    this._subtitleText2.anchor.set(0.5, 0.5)
    this._subtitleText2.fixedToCamera = true
    this._positionSubtitleText()
    this._hideSubtitleText()

    this._subtitleImage = null;

    this._leftExitButton = this.game.add.button(0, 0, 'exitButton',
      this.state.shutdown,
      this.state
    )
    this._leftExitButton.anchor.setTo(0.5, 0.5)
    this._leftExitButton.fixedToCamera = true
    this._positionLeftExitButton()
    this._hideLeftExitButton()

    this._spectateButton = this.game.add.button(0, 0, 'spectateButton',
      this._spectateButtonPressed,
      this
    )
    this._spectateButton.anchor.setTo(0.5, 0.5)
    this._spectateButton.fixedToCamera = true
    this._positionSpectateButton()
    this._hideSpectateButton()

    this._footerText = this.game.add.text(0, 0, '', {
      font: 'bold 20pt ' + this.FONT,
      fill: '#FFFFFF',
      align: 'center'
    })
    this._footerText.setShadow(2, 2, 'rgba(0,0,0,0.5)', 5)
    this._footerText.anchor.set(0.5, 0.5)
    this._footerText.fixedToCamera = true
    this._positionFooterText()    
    this._hideFooterText()

    return this
  }

  /****************
   * Main Methods *
   ****************/  
  this.timerEnded = function() {
    this._showMask()
    this._showTitleText("Game Over!")
    //this._showSubtitleText("Out of Time...")
    this._showLeaderboard()
    this._showExitButton()
    this.active = true
    this.state.scoreboard.toggleVisibility(0);
    this._positionGoogleAd(true);
  }

  this.wonGame = function() {
    this._showMask()
    this._showTitleText("You Survived!")
    this._showLeaderboard()
    this._showExitButton()
    this.active = true
    this.state.scoreboard.toggleVisibility(0);
    this._positionGoogleAd(true);
  }

  this.died = function(diedText, person, type, score) {
    this._showMask()
    this._showTitleText("You Died!")
    if (diedText === this.state.KILLED_YOURSELF_TEXT) {
      this._showSubtitleText(diedText);
    } else {
      this._showSubtitleText(diedText, [person, type, score]);
    }
    this._showLeftExitButton()
    this._showSpectateButton()
    this.active = true
    this.state.scoreboard.toggleVisibility(0);
    this._positionGoogleAd(true);
  }

  this.spectate = function() {
    this._showFooterText("Press Q to Exit")
    this._positionGoogleAd(false);
  }

  this.reposition = function() {
    this._drawMask()
    this._positionExitButton()
    this._positionLeaderboard()
    this._positionTitleText()
    this._positionSubtitleText()
    this._positionLeftExitButton()
    this._positionSpectateButton()
    this._positionFooterText()
    this._positionGoogleAd(this.active ? true : false)
  }

  /*******************
   * Private Methods *
   *******************/  
  this._drawMask = function() {
    this._mask.clear()
    this._mask.lineStyle(0, 0x000000)
    this._mask.beginFill(0x000000, 0.65)
    this._mask.drawRect(0, 0, this.state.GAME_WIDTH, this.state.GAME_HEIGHT)
    this._mask.endFill()
  }

  this._showMask = function() {
    this._mask.visible = true
  }

  this._hideMask = function() {
    this._mask.visible = false
  }

  this._positionExitButton = function() {
    this._exitButton.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2,
      4 * this.state.GAME_HEIGHT / 5
    )
  }

  this._hideExitButton = function() {
    this._exitButton.visible = false
  }

  this._showExitButton = function() {
    this._exitButton.visible = true
  }

  this._positionLeaderboard = function() {
    this._leaderboardText.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2,
      this.state.GAME_HEIGHT / 2
    )
  }

  this._showLeaderboard = function() {
    if (this._leaderboardText.visible) {
      return
    }

    this._leaderboardText.visible = true

    this.game.time.events.add(
      1500, 
      function() {
        var k, leaderboard, score, scoreString;
        scoreString = 'Final Scores\n';
        leaderboard = this.game.data.state.leaderboard;
        for (k = 0; k < leaderboard.length; k++) {
          score = leaderboard[k];
          var tankName = this.game.data.state.nicknames[score[0].id];
          scoreString += tankName + ": " + score[1] + "\n";
        }
        this._leaderboardText.setText(scoreString);
      }, 
      this
    )    
  }

  this._hideLeaderBoard = function() {
    this._leaderboardText.visible = false
    this._leaderboardText.setText("Calculating Scores...")
  }

  this._positionTitleText = function() {
    this._titleText.cameraOffset.setTo(
      this.game.width / 2,
      this.game.height / 6
    );
  }

  this._hideTitleText = function() {
    this._titleText.visible = false
  }

  this._showTitleText = function(title) {
    if (title) {
      this._titleText.setText(title)
    }
    this._titleText.visible = true
  }

  this._positionSubtitleText = function() {
    this._subtitleText.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2,
      this.state.GAME_HEIGHT / 4 + this._titleText.height
    );
    this._subtitleText2.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2 - 35,
      this.state.GAME_HEIGHT / 4 + this._titleText.height + this._subtitleText.height
    );
    if (this._subtitleImage) {
      this._subtitleImage.cameraOffset.setTo(
        this.state.GAME_WIDTH / 2 + (this._subtitleText2.width * 0.5) + 20,
        this.state.GAME_HEIGHT / 4 + this._titleText.height + this._subtitleText.height
      );
    }
  }

  this._hideSubtitleText = function() {
    this._subtitleText.visible = false
    this._subtitleText2.visible = false;
    if (this._subtitleImage) {
      this._subtitleImage.visible = false;
    }
  }

  this._positionLeftExitButton = function() {
    this._leftExitButton.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2 + 150,
      4 * this.state.GAME_HEIGHT / 5
    )
  }

  this._showSubtitleText = function(subtitle, args) {
    if (subtitle) {
      if (args) {
        this._subtitleText.setText("Score: " + args[2] + "\n" + subtitle);
        this._subtitleText2.setText(args[0]);
        this._subtitleImage = this.game.add.image(0, 0, args[1]);
        this._subtitleImage.width = 42;
        this._subtitleImage.height = 42;
        this._subtitleImage.anchor.setTo(0, 0.5);
        this._subtitleImage.fixedToCamera = true;
        this._subtitleImage.visible = true;
      } else {
        this._subtitleText.setText(subtitle);
      }
    }
    this._subtitleText.visible = true;
    this._subtitleText2.visible = true;
    this._positionSubtitleText();
  }

  this._hideLeftExitButton = function() {
    this._leftExitButton.visible = false
  }

  this._showLeftExitButton = function() {
    this._leftExitButton.visible = true
  }

  this._positionSpectateButton = function() {
    this._spectateButton.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2 - 150,
      4 * this.state.GAME_HEIGHT / 5
    )
  }

  this._hideSpectateButton = function() {
    this._spectateButton.visible = false
  }

  this._showSpectateButton = function() {
    this._spectateButton.visible = true    
  }

  this._spectateButtonPressed = function() {
    this._hideMask()
    this._hideTitleText()
    this._hideSubtitleText()
    this._hideLeftExitButton()
    this._hideSpectateButton()
    this.state.startSpectateMode()
    this.active = false
    this.state.scoreboard.toggleVisibility(1);
  }

  this._positionFooterText = function() {
    this._footerText.cameraOffset.setTo(
      this.state.GAME_WIDTH / 2,
      this.state.GAME_HEIGHT - 20
    )
  }

  this._hideFooterText = function() {
    this._footerText.visible = false
  }

  this._showFooterText = function(text) {
    if (text) { this._footerText.setText(text) }
    this._footerText.visible = true
  }

  this._positionGoogleAd = function(show) {
    if (show) {
      $('#googleAd').show();
    } else {
      $('#googleAd').hide();
    }
  }
}
