/**
 * @file health-streak-bars.js
 * Class definition for the health and streak bars in the game state.
 *
 * @project plip.io
 * @author
 *   Pranay Kumar
 *   Rohit Lalchandani
 */
function GameBars(gameState) {
  this.state = gameState;
  this.game = gameState.game;

  this.init = function() {
    this.HIT_STREAK_THRESHOLD = 10;

    this.STREAK_FRACTION = 1 / this.HIT_STREAK_THRESHOLD;
    this.HEALTH_FRACTION = 1 / this.state.myTank.STARTING_HEALTH;

    this.BAR_TEXT_STYLE = { font: "bold 12pt Orbitron", align: 'center' }
    this.GAME_BARS_BOX_WIDTH = 140
    this.GAME_BARS_BOX_HEIGHT = 200
    this.STREAK_BAR_RIGHT_PADDING = this.state.permanentIDs.length >= 7 ? this.state.GAME_WIDTH - 40 : 100
    this.HEALTH_BAR_RIGHT_PADDING = this.state.permanentIDs.length >= 7 ? this.state.GAME_WIDTH - 100 : 40
    this.BOX_Y = 350;
    this.BAR_TOP_PADDING = this.BOX_Y + 15;
    this.BAR_WIDTH = 25
    this.FILL_WIDTH = this.BAR_WIDTH - 8
    this.BAR_HEIGHT = 125
    this.FILL_HEIGHT = this.BAR_HEIGHT - 12

    this._box = this.game.add.graphics(0, 0);
    this._box.fixedToCamera = true;
    this._drawBox()

    this._streakBar = this.game.add.image(0, 0, 'bar');
    this._streakBar.width = this.BAR_WIDTH
    this._streakBar.height = this.BAR_HEIGHT
    this._streakBar.fixedToCamera = true;
    this._streakBar.anchor.setTo(0.5, 0)
    this._streakLabel = this.game.add.text(0, 0, 'Hits', this.BAR_TEXT_STYLE);
    this._streakLabel.fixedToCamera = true;
    this._streakLabel.anchor.setTo(0.5, 0)
    this._streakText = this.game.add.text(0, 0, '', this.BAR_TEXT_STYLE);
    this._streakText.fixedToCamera = true;
    this._streakText.anchor.setTo(0.5, 1);
    this._streakBarFill = this.game.add.graphics()
    this._streakBarFill.fixedToCamera = true;
    this._streakBarFill.beginFill(0x00FF00, 1);
    this._streakBarFill.drawRect(0, 0, this.FILL_WIDTH, 0);
    this._streakBarFill.endFill();
    this._positionStreakBar()

    this._healthBar = this.game.add.image(0, 0, 'bar');
    this._healthBar.width = this.BAR_WIDTH
    this._healthBar.height = this.BAR_HEIGHT
    this._healthBar.fixedToCamera = true;
    this._healthBar.anchor.setTo(0.5, 0)
    this._healthLabel = this.game.add.text(0, 0, 'Health', this.BAR_TEXT_STYLE);
    this._healthLabel.fixedToCamera = true;
    this._healthLabel.anchor.setTo(0.5, 0)
    this._healthText = this.game.add.text(0, 0, '', this.BAR_TEXT_STYLE)
    this._healthText.fixedToCamera = true;
    this._healthText.anchor.setTo(0.5, 1);
    this._healthBarFill = this.game.add.graphics()
    this._healthBarFill.fixedToCamera = true;
    this._healthBarFill.beginFill(0x00FF00, 1);
    this._healthBarFill.drawRect(0, 0, this.FILL_WIDTH, 0);
    this._healthBarFill.endFill();
    this._positionHealthBar()

    return this
  }


  this.refresh = function() {
    var health = this.game.data.state[network.currentUser()].health
    var streak = this.game.data.state[this.state.myTank.id].streak;
    var hits = streak % this.HIT_STREAK_THRESHOLD;
    if (streak > 0 && hits === 0) {
      hits = this.HIT_STREAK_THRESHOLD;
    }

    this._healthText.setText(health)
    this._streakText.setText(streak)

    this._streakBarFill.clear();
    this._streakBarFill.beginFill(0x00FF00, 1);
    
    this._streakBarFill.drawRect(0, 0, this.FILL_WIDTH, -(this.FILL_HEIGHT * (hits * this.STREAK_FRACTION)));
    this._streakBarFill.endFill();

    this._healthBarFill.clear();
    if (health <= (0.2 * this.state.myTank.STARTING_HEALTH)) {
      this._healthBarFill.beginFill(0xFF0000, 1);
    } else if (health <= (0.5 * this.state.myTank.STARTING_HEALTH)) {
      this._healthBarFill.beginFill(0xFFFF00, 1);
    } else {
      this._healthBarFill.beginFill(0x00FF00, 1);
    }
    this._healthBarFill.drawRect(0, 0, this.FILL_WIDTH, -(this.FILL_HEIGHT * (health * this.HEALTH_FRACTION)));
    this._healthBarFill.endFill();
  }

  this.reposition = function() {
    this._box.clear()
    this._drawBox()
    this.STREAK_BAR_RIGHT_PADDING = this.state.permanentIDs.length >= 7 ? this.state.GAME_WIDTH - 40 : 100
    this.HEALTH_BAR_RIGHT_PADDING = this.state.permanentIDs.length >= 7 ? this.state.GAME_WIDTH - 100 : 40
    this._positionStreakBar()
    this._positionHealthBar()
  }

  this._drawBox = function() {
    var cameraX = this.state.permanentIDs.length >= 7 ? 0 : this.state.GAME_WIDTH - this.GAME_BARS_BOX_WIDTH;
    this._box.cameraOffset.setTo(cameraX, 320);
    this._box.lineStyle(this.state.MENU_BORDER_WIDTH, this.state.MENU_BORDER_COLOR);
    this._box.beginFill(this.state.MENU_FILL_COLOR, this.state.MENU_FILL_ALPHA);
    this._box.drawRect(0, 0, this.GAME_BARS_BOX_WIDTH, this.GAME_BARS_BOX_HEIGHT);
    this._box.endFill();
  }

  this._positionStreakBar = function() {
    this._streakBar.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.STREAK_BAR_RIGHT_PADDING, 
      this.BAR_TOP_PADDING
    )    
    this._streakLabel.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.STREAK_BAR_RIGHT_PADDING, 
      this._streakBar.cameraOffset.y + this.BAR_HEIGHT
    )
    this._streakText.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.STREAK_BAR_RIGHT_PADDING,
      this._streakBar.cameraOffset.y
    )
    this._streakBarFill.cameraOffset.setTo(
      this._streakBar.cameraOffset.x - (this.BAR_WIDTH / 2) + 5,
      this._streakBar.cameraOffset.y + this.BAR_HEIGHT - 5
    )
  }

  this._positionHealthBar = function() {
    this._healthBar.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.HEALTH_BAR_RIGHT_PADDING,
      this.BAR_TOP_PADDING
    )    
    this._healthLabel.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.HEALTH_BAR_RIGHT_PADDING,
      this._healthBar.cameraOffset.y + this.BAR_HEIGHT
    )    
    this._healthText.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.HEALTH_BAR_RIGHT_PADDING,
      this._healthBar.cameraOffset.y
    )
    this._healthBarFill.cameraOffset.setTo(
      this._healthBar.cameraOffset.x - (this.BAR_WIDTH / 2) + 5,
      this._healthBar.cameraOffset.y + this.BAR_HEIGHT - 5
    )
  }
}
