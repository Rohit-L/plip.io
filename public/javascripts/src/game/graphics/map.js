/**
 * @file map.js
 * Class definition for game map
 *
 * @project plip.io
 * @author
 *   Pranay Kumar
 *   Rohit Lalchandani
 */
 function GameMap(gameState) {
  this.state = gameState;
  this.game = gameState.game;

  this.init = function() {
    this.SAFE_TILE = 1
    this.WALL_TILE = 2
    this.CRACKED_TILE = 3
    this.EXTRA_CRACKED_TILE = 4

    this.TILEMAP_MAIN_LAYER = "layer";
    this.TILEMAP_WALLS_LAYER = "walls";
    this.TILESET_NAME = "orangeTiles";

    this._map = this.state.add.tilemap('map');
    this._map.addTilesetImage(this.TILESET_NAME, 'tiles')
    this.layer = this._map.createLayer(this.TILEMAP_MAIN_LAYER)
    this.layer.renderSettings.enableScrollDelta = false
    this._map.setCollision(this.WALL_TILE, true, this.layer)
    this._map.setCollision(this.CRACKED_TILE, true, this.layer)
    this._map.setCollision(this.EXTRA_CRACKED_TILE, true, this.layer)

    return this;
  };

  this.crackTile = function(data) {
    var tile = this._map.getTile(data.tileX, data.tileY);
    if (data.health === 2) {
      this._map.putTile(this.CRACKED_TILE, data.tileX, data.tileY, this.layer);
    } else if (data.health === 1) {
      this._map.putTile(this.EXTRA_CRACKED_TILE, data.tileX, data.tileY, this.layer);
    } else if (data.health === 0) {
      var crumble = this.game.add.sprite(tile.worldX + (this.state.GRID_SIZE / 2), tile.worldY + (this.state.GRID_SIZE / 2), 'crumble')
      crumble.width = 128; crumble.height = 128
      crumble.anchor.setTo(0.5, 0.5)
      crumble.animations.add('crumbleAnimation', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 18, true)
      crumble.animations.play('crumbleAnimation', null, false, true);
      this.state.sounds.wallCrack();
      this._map.putTile(this.SAFE_TILE, data.tileX, data.tileY, this.layer)
    }
  }

  this.getTiles = function(marker) {
    var adjacentTiles = [null, null, null, null, null]
    adjacentTiles[1] = this._map.getTileLeft(this.layer.index, marker.x, marker.y)
    adjacentTiles[2] = this._map.getTileRight(this.layer.index, marker.x, marker.y)
    adjacentTiles[3] = this._map.getTileAbove(this.layer.index, marker.x, marker.y)
    adjacentTiles[4] = this._map.getTileBelow(this.layer.index, marker.x, marker.y)
    return adjacentTiles
  }

  this.getTileXY = function(projectile) {
    return this._map.getTileWorldXY(projectile.x, projectile.y)
  }

  this.getTile = function(x, y) {
    return this._map.getTile(x, y)
  }
}
