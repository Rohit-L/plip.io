/**
 * @file minimap.js
 * Class definition for the minimap in the game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function Minimap(gameState) {
  this.state = gameState
  this.game = gameState.game
  this.BOX_SIZE = 150;

  this._box = this.game.add.graphics(0, 0);
  this._box.fixedToCamera = true;
  this._box.cameraOffset.setTo(0, 0);

  this.refresh = function() {
    var id, j, len, ids, coordinates, x, y;

    this._box.clear();
    this._box.lineStyle(this.state.MENU_BORDER_WIDTH, this.state.MENU_BORDER_COLOR);
    this._box.beginFill(this.state.MENU_FILL_COLOR, this.state.MENU_FILL_ALPHA);
    this._box.drawRect(0, 0, this.BOX_SIZE, this.BOX_SIZE);
    this._box.endFill();
    this._box.lineStyle(2, 0xCC0000);
    this._box.beginFill(0xCC0000, 1);

    // Draw Enemy Tanks
    ids = this.game.data.state.playerIDs;
    for (j = 0, len = ids.length; j < len; j++) {
      id = ids[j];
      if (this.state.allTanks[id] === this.state.myTank || !this.state.allTanks[id] || !this.state.allTanks[id].alive) {
        continue;
      }

      coordinates = this.state.allTanks[id].tileCoordinates()
      x = coordinates[0] * 6
      y = coordinates[1] * 6;

      this._box.drawCircle(x, y, 7);
    }

    this._box.endFill();

    // Draw My Tank
    if (this.state.myTank.alive) {
      this._box.lineStyle(2, 0x1DD91D);
      this._box.beginFill(0x1DD91D, 1);
      
      coordinates = this.state.myTank.tileCoordinates()
      x = coordinates[0] * 6
      y = coordinates[1] * 6;

      this._box.drawCircle(x, y, 7);
    }

    if (this.state.spectateMode) {
      this._box.lineStyle(2, 0x1DD91D);
      this._box.beginFill(0x1DD91D, 0.4);
      var gridSize = this.state.GRID_SIZE;
      
      coordinates = [
        this.game.math.snapToFloor(this.state.smoothCamera.getX(), gridSize) / gridSize, 
        this.game.math.snapToFloor(this.state.smoothCamera.getY(), gridSize) / gridSize
      ];

      x = coordinates[0];
      y = coordinates[1];

      var inverseGridAndBoardSize = this.state.INVERSE_BOARD_SIZE * this.state.INVERSE_GRID_SIZE;

      this._box.drawRect(
        this.BOX_SIZE * this.state.INVERSE_BOARD_SIZE * x,
        this.BOX_SIZE * this.state.INVERSE_BOARD_SIZE * y,
        this.BOX_SIZE * this.state.GAME_WIDTH * inverseGridAndBoardSize,
        this.BOX_SIZE * this.state.GAME_HEIGHT * inverseGridAndBoardSize
      )
    }
  }
}
