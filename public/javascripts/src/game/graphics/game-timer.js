/**
 * @file game-timer.js
 * Class definition for the game timer in the game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function GameTimer(gameState) {
  this.state = gameState
  this.game = gameState.game
  
  this.init = function() {
    this.GAME_TIMER_STYLE = { 
      font: "25pt Orbitron",
      align: "center",
      backgroundColor: 'rgba(255, 255, 255, ' + this.state.MENU_FILL_ALPHA + ')',
      fill: 'black'
    };
    this.GAME_TIMER_STYLE_PRE_GAME = { 
      font: "25pt Orbitron",
      align: "center",
      backgroundColor: 'rgba(255, 255, 255, 0)',
      fill: 'white'
    };

    this.GAME_TIMER_HEIGHT = 10;
    this.MINUTES_PER_SECOND = 1 / 60;

    this._text = this.game.add.text(0, 0, '', this.GAME_TIMER_STYLE);
    this._text.fixedToCamera = true;
    this._text.anchor.setTo(0.5, 0);
    this._positionText();

    return this;
  };

  /****************
   * Main Methods *
   ****************/
  this.refresh = function() {
    var timeStamp = this.game.data.state.time;
    if (timeStamp) {
      var time = timeStamp.remaining - (Math.round((Date.now() - timeStamp.set) / 1000))
      if (timeStamp.playing) {
        this._text.setText(this._format(time >= 0 ? time : 0));
        this._positionText();
      } else {
        this._text.setText("Starting in " + this._format(time >= 0 ? time : 0));
        this._positionText("middle");
      }
    }
  }

  this.reposition = function() {
    this._positionText();
  }

  /*******************
   * Private Methods *
   *******************/
  this._format = function(seconds) {
    var minutes = "0" + Math.floor(seconds * this.MINUTES_PER_SECOND);
    var remainingSeconds = "0" + (seconds - minutes * 60);
    return minutes.substr(-2) + ":" + remainingSeconds.substr(-2);   
  }

  this._positionText = function(location) {
    if (location === "middle") {
      this._text.cameraOffset.setTo(this.state.GAME_WIDTH / 2, this.state.GAME_HEIGHT / 2)
      this._text.setStyle(this.GAME_TIMER_STYLE_PRE_GAME)
      return;
    }

    this._text.setStyle(this.GAME_TIMER_STYLE)
    if (this.state.GAME_WIDTH > 875) {
      this._text.cameraOffset.setTo(this.state.GAME_WIDTH / 2, this.GAME_TIMER_HEIGHT);
    } else {
      this._text.cameraOffset.setTo(60, 150);
    }
  }
}
