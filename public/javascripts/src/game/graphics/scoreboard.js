/**
 * @file new-scoreboard.js
 * Class definition for the new scoreboard (coming soon) in the game state.
 *
 * @project plip.io
 * @author
 *   Pranay Kumar
 *   Rohit Lalchandani
 */

function Scoreboard(gameState) {
  this.state = gameState;
  this.game = gameState.game;

  this.init = function() {
    this.INFO_BOX_WIDTH = this.state.GAME_WIDTH > 560 ? 250 : 150;
    this.PADDING = 20;
    this.LEADERBOARD_NUM_ROWS = this.state.permanentIDs.length;
    this.INFO_BOX_HEIGHT = 2 * this.PADDING * this.LEADERBOARD_NUM_ROWS;
    this.TANK_OFFSET = 14;
    this.TANK_DIM = 26;

    this._createBox();
    this._createTanks();

    return this;
  }

  this._createBox = function() {
    this._box = this.game.add.graphics(0, 0);
    this._box.fixedToCamera = true;
    this._box.cameraOffset.setTo(this.state.GAME_WIDTH - this.INFO_BOX_WIDTH, 0);
    this._box.lineStyle(this.state.MENU_BORDER_WIDTH, this.state.MENU_BORDER_COLOR);
    this._box.beginFill(this.state.MENU_FILL_COLOR, this.state.MENU_FILL_ALPHA);
    this._box.drawRect(0, 0, this.INFO_BOX_WIDTH, this.INFO_BOX_HEIGHT);
    this._box.endFill();
  };

  this._createTanks = function() {
    this._tanks = [];
    var ids = this.state.permanentIDs;
    for (var i = 0; i < ids.length; i++) {
      var id = ids[i];
      var tank = this.state.permanentTanks[id];
      if (tank) {
        var offset = this._positionRow(i);
        var scoreboardTank = new ScoreboardTank(this, tank, offset[0], offset[1]).init();
        this._box.beginFill(0x000000, 1);
        this._box.drawRect(this.PADDING * 0.5, this.PADDING * (0.5 + (2 * i)), 4, this.TANK_DIM);
        this._box.endFill();
        this._tanks.push(scoreboardTank);
      }
    }
  };  

  this.refresh = function() {
    var err, error, i, id, j, len, myTankIndex, ids, scores, tank;

    var scores = [];
    var ids = this.state.permanentIDs;
    for (j = 0, len = ids.length; j < len; j++) {
      id = ids[j];
      var tank = this.state.permanentTanks[id];
      if (tank) {
        scores.push([tank, this.game.data.state[id].score || 0]);
      }
    }

    /* Sort scores based on score number */
    scores.sort(function(a, b) { return b[1] - a[1]; });
    this.maxScore = scores[0][1];

    this.game.data.state['leaderboard'] = scores;
    try {
      for (var i = 0; i < this._tanks.length; i++) {
        var scoreboardTank = this._tanks[i];
        scoreboardTank.tank = scores[i][0];
        scoreboardTank.refresh(this.maxScore);
      }
    } catch (error) {
      return;
    }
  }

  this._positionRow = function(rowNumber) {
    var offset = [];
    offset.push(this._box.cameraOffset.x + this.TANK_OFFSET);
    offset.push(this._box.cameraOffset.y + (this.PADDING * 0.5) + (this.INFO_BOX_HEIGHT * (rowNumber / this.LEADERBOARD_NUM_ROWS)));
    return offset;
  };

  this.toggleVisibility = function(alpha) {
    this._box.alpha = alpha;
    for (var i = 0; i < this._tanks.length; i++) {
      var scoreboardTank = this._tanks[i];
      scoreboardTank.toggleVisibility(alpha);
    }
  };

  this.reposition = function() {
    this.INFO_BOX_WIDTH = this.state.GAME_WIDTH > 560 ? 250 : 150;
    this._box.clear()
    this._createBox();
    for (var i = 0; i < this._tanks.length; i++) {
      this._tanks[i].kill();
    }
    this._createTanks();
    if (this.state.overlays.active) {
      this.toggleVisibility(0);
    }
  };

}
