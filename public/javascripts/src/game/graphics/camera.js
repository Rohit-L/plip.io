/**
 * @file camera.js
 * Class definition for the camera in the game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function Camera(gameState) {
  this.state = gameState
  this.game = gameState.game
  this._phaserCamera = gameState.camera

  this._position = new Phaser.Point(this.state.myTank.x, this.state.myTank.y)

  this.getX = function() {
    return this._phaserCamera.position.x;
  }

  this.getY = function() {
    return this._phaserCamera.position.y;
  }

  this.refresh = function() {
    var lerp;
    if (!this.state.spectateMode) {
      lerp = 0.1
      this._position.x += (this.state.myTank.x - this._position.x) * lerp
      this._position.y += (this.state.myTank.y - this._position.y) * lerp
      this._phaserCamera.focusOnXY(this._position.x, this._position.y);
    }
  }

  this.keyAdjustment = function() {
    var speed = 5;
    var x = this._phaserCamera.position.x;
    var y = this._phaserCamera.position.y;
    if (this.state.cursors.down.isDown) {
      this._phaserCamera.setPosition(x, y + speed);
    }
    if (this.state.cursors.up.isDown) {
      this._phaserCamera.setPosition(x, y - speed);
    }
    if (this.state.cursors.right.isDown) {
      this._phaserCamera.setPosition(x + speed, y);
    }
    if (this.state.cursors.left.isDown) {
      this._phaserCamera.setPosition(x - speed, y);
    }
  }
}
