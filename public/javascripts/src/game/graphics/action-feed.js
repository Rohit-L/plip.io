/**
 * @file action-feed.js
 * Class definition for the streak bar in the game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function ActionFeed(gameState) {
  this.state = gameState
  this.game = gameState.game

  this.init = function() {
    this.ACTION_FEED_HEIGHT = 125
    this.ACTION_FEED_WIDTH = 320
    this.ACTION_FEED_NUM_ROWS = 3
    this.ACTION_FEED_PADDING = 8
    this.ACTION_FEED_ROW_STYLE = { font: "15pt Orbitron", align: 'center', backgroundColor: 'rgba(255, 255, 255, .5)', fill: 'black' };
    this.ACTION_FEED_SPEED = 250
      
    this._rows = []
    for (var i = 0; i < this.ACTION_FEED_NUM_ROWS; i++) {
      var row = this.game.add.text(0, 0, '', this.ACTION_FEED_ROW_STYLE)
      row.fixedToCamera = true
      row.anchor.setTo(1, 0)
      this._setRowCameraOffset(row, i)
      this._rows.push(row)
    }

    this._invisibleRow = this.game.add.text(0, 0, '', this.ACTION_FEED_ROW_STYLE)
    this._invisibleRow.fixedToCamera = true
    this._invisibleRow.anchor.setTo(1, 0)
    this._setRowCameraOffset(this._invisibleRow, this.ACTION_FEED_NUM_ROWS)
    this._invisibleRow.alpha = 0

    this._queue = []
    this._active = false

    return this
  }

  this.appendAction = function(data) {
    var shooterID = data[0]; var victimID = data[1]; var type = data[2];

    var shooterName = shooterID === this.state.myTank.id ? 'You' : this.game.data.state.nicknames[shooterID]
    var victimName = victimID === this.state.myTank.id ? 'you' : this.game.data.state.nicknames[victimID]

    if (shooterID === victimID) {
      var text = shooterName + " " + type
      text += shooterID === this.state.myTank.id ? " yourself. Idiot" : " themselves. Idiot."
    } else {
      var text = shooterName + " " + type + " " + victimName
    }

    this._queue.push(" " + text + " ")
  }

  this.appendNotice = function(data) {
    var tankID = data[0]; var type = data[1]; var args = data[2];
    var shooterName = tankID === this.state.myTank.id ? "You" : this.game.data.state.nicknames[tankID]

    switch (type) {
      case 'death':
        var text = shooterName;
        text += shooterName === 'You' ? ' have been killed.' : ' has been killed.';
        this._queue.push(" " + text + " ")
        break;
      case 'streak':
        var text = shooterName;
        text += shooterName === "You" ? ' have ' : ' has ';
        text += args[0] + ' total hits.';
        this._queue.push(" " + text + " ")
        break;
      case 'bonus':
        this._queue.push(" You are on fire with " + args[0] + " hits. ");
        this._queue.push(" You received 5 more super bullets. ");
        this._queue.push(" You received 5 more points. ");
        break;
      default:
        null;
    }
  }

  this.refresh = function() {
    if (this._queue.length < 1 || this._active) { return }

    this._active = true;
    var text = this._queue.shift();

    // Move everything up
    for (var i = 0; i < this.ACTION_FEED_NUM_ROWS; i++) {
      this.game.add.tween(this._rows[i].cameraOffset).to( {
        y: this._rows[i].cameraOffset.y - 
              ((this.ACTION_FEED_HEIGHT - (2 * this.ACTION_FEED_PADDING)) / this.ACTION_FEED_NUM_ROWS)
      }, this.ACTION_FEED_SPEED, "Linear", true)
    }

    this.game.add.tween(this._invisibleRow.cameraOffset).to( {
      y: this._invisibleRow.cameraOffset.y - 
            ((this.ACTION_FEED_HEIGHT - (2 * this.ACTION_FEED_PADDING)) / this.ACTION_FEED_NUM_ROWS)
    }, this.ACTION_FEED_SPEED, "Linear", true)

    // New set of visible rows
    var newLiveFeedRows = [this._rows[1], this._rows[2], this._invisibleRow]
    newLiveFeedRows[2].setText(text)

    if (newLiveFeedRows[2].activeTimer) { newLiveFeedRows[2].activeTimer.destroy() }
    newLiveFeedRows[2].activeTimer = this.game.time.create(false)
    newLiveFeedRows[2].activeTimer.add(5000, function() {
      this.game.add.tween(this).to({ 
        alpha: 0 
      }, this.game.state.getCurrentState().actionFeed.ACTION_FEED_SPEED, "Linear", true)
    }, newLiveFeedRows[2])
    newLiveFeedRows[2].activeTimer.start()

    // Set next invisible row
    this._invisibleRow = this._rows[0]

    // Fading Animations
    var fadeOut = this.game.add.tween(this._invisibleRow).to( {
      alpha: 0
    }, this.ACTION_FEED_SPEED, "Linear", true)
    fadeOut.onComplete.add(function() {
      this._invisibleRow.cameraOffset.y = this.state.GAME_HEIGHT - this.ACTION_FEED_HEIGHT + this.ACTION_FEED_PADDING + 
        ((this.ACTION_FEED_HEIGHT - (2 * this.ACTION_FEED_PADDING)) / this.ACTION_FEED_NUM_ROWS) * this.ACTION_FEED_NUM_ROWS
      this._active = false  
    }, this)

    this.game.add.tween(newLiveFeedRows[2]).to( {
      alpha: 1
    }, this.ACTION_FEED_SPEED, "Linear", true)

    // Set new visible rows
    this._rows = newLiveFeedRows
  }

  this.reposition = function() {
    for (var i = 0; i < this.ACTION_FEED_NUM_ROWS; i++) {
      var row = this._rows[i]
      this._setRowCameraOffset(row, i)
    }
    this._setRowCameraOffset(this._invisibleRow, this.ACTION_FEED_NUM_ROWS)
  }

  this._setRowCameraOffset = function(row, rowNumber) {
    row.cameraOffset.setTo(
      this.state.GAME_WIDTH - this.ACTION_FEED_PADDING,
      this.state.GAME_HEIGHT - this.ACTION_FEED_HEIGHT + this.ACTION_FEED_PADDING + 
          ((this.ACTION_FEED_HEIGHT - (2 * this.ACTION_FEED_PADDING)) / this.ACTION_FEED_NUM_ROWS) * rowNumber
    )
  }
}
