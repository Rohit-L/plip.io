/**
 * @file sounds.js
 * Class definition for all the sounds of the game
 *
 * @project plip.io
 * @author
 *   Pranay Kumar
 *   Rohit Lalchandani
 */
 function Sounds(gameState) {
  this.state = gameState;
  this.game = gameState.game;

  this.init = function() {
    this._music = this.game.add.audio('music');
    this._bulletFiredSound = this.game.add.audio('bulletFired');
    this._bulletHitSound = this.game.add.audio('bulletHit');
    this._bombExplodedSound = this.game.add.audio('bombExplosion');
    this._crackSound = this.game.add.audio('crack')
    this._beepSound = this.game.add.audio('beep');

    return this;
  };

  this.playMusic = function() {
    if (window.gameSettings.music) {
      this._music.play("", 0, 1, true);
    }
  };

  this.destroyMusic = function() {
    this._music.destroy();
  }

  this.bulletFire = function() {
    if (window.gameSettings.sounds) {
      this._bulletFiredSound.play();
    }
  };

  this.bulletCollision = function() {
    if (window.gameSettings.sounds) {
      this._bulletHitSound.play();
    }
  }

  this.explosion = function() {
    if (window.gameSettings.sounds) {
      this._bombExplodedSound.play();
    }
  };

  this.wallCrack = function() {
    if (window.gameSettings.sounds) {
      this._crackSound.play();
    }
  };

  this.beep = function() {
    if (window.gameSettings.sounds) {
      this._beepSound.play();
    }
  };
}
