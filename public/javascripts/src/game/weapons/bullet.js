/**
 * @file bullet.js
 * Class definition for bullets
 *
 * @project plip.io
 * @author
 *   Pranay Kumar
 *   Rohit Lalchandani
 */
function Bullet(game, x, y, type) {
  if (type === 'megaBullet') {
    this.BULLET_TYPE = 'megaBullet'
    this.BULLET_WIDTH = 28
    this.BULLET_HEIGHT = 28
  } else {
    this.BULLET_TYPE = 'bullet'
    this.BULLET_WIDTH = 12
    this.BULLET_HEIGHT = 16
  }

  Phaser.Sprite.call(this, game, x, y, this.BULLET_TYPE)
  game.add.existing(this)

  this.state = game.state.getCurrentState()
  this.width = this.BULLET_WIDTH;
  this.height = this.BULLET_HEIGHT;
  this.anchor.set(0.5, 0.5)
  
  this.refresh = function(x, y, direction, newBullet) {
    if (newBullet) {
      this.x = x; this.y = y;
    } else {
      this.game.add.tween(this).to({ x: x, y: y}, 25, "Linear", true);
    }
    this.alive = true
    this.visible = true
    if (this.BULLET_TYPE === 'megaBullet') { 
      this._setMegaBulletAngle(direction);
    } else {
      this._setBulletAngle(direction);
    }
  }

  this.collided = function() {
    this.id = null;
    this.alive = false;
    this.kill();
    if (this.BULLET_TYPE === 'megaBullet') {
      this._createExplosion();
      this.state.sounds.explosion();
    } else {
      this.state.sounds.bulletCollision();
    }
  }

  this._setMegaBulletAngle = function(direction) {
    if (direction === Phaser.LEFT) { this.angle = 180; }
    if (direction === Phaser.UP) { this.angle = -90; }
    if (direction === Phaser.DOWN) { this.angle = 90; }
  }

  this._setBulletAngle = function(direction) {
    if (direction === Phaser.RIGHT) { this.angle = 90; }
    if (direction === Phaser.LEFT) { this.angle = -90; }
    if (direction === Phaser.DOWN) { this.angle = 180; }
  }

  this._createExplosion = function() {
    var explode = this.game.add.sprite(this.x, this.y, 'explosion');
    explode.width = 256; explode.height = 256;
    explode.anchor.setTo(0.5, 0.5);
    explode.animations.add('explode', 
      [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
        32, 32, 33, 34, 35, 36
      ],
      36, true
    );
    explode.animations.play('explode', null, false, true);      
  }
};

Bullet.prototype = Object.create(Phaser.Sprite.prototype);
Bullet.prototype.constructor = Bullet;
