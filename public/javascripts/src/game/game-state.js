/**
 * @file game-state.js
 * Class definition for the main game state.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
window.GameState = function() {
  return {
    init: function() {
      this.GRID_SIZE = 64;
      this.INVERSE_GRID_SIZE = 1 / 64;
      this.BOARD_SIZE = 25;
      this.INVERSE_BOARD_SIZE = 0.04;
      this.WORLD_WIDTH = this.BOARD_SIZE * this.GRID_SIZE;
      this.WORLD_HEIGHT = this.WORLD_WIDTH;

      this.NUM_BOMBS = 20;
      this.NUM_BULLETS = 500;

      this.MENU_FILL_COLOR = 0xFFFFFF;
      this.MENU_FILL_ALPHA = 0.3;
      this.MENU_BORDER_WIDTH = 0;
      this.MENU_BORDER_COLOR = 0x7A7976;

      this.TILEMAP_FILE = "data/" + Game.data.state.map;
      this.TILE_COLORS = ['blue', 'orange', 'purple', 'green', 'red'];
      this.TILESET_FILE = 'data/' + 
          this.TILE_COLORS[Math.floor(Math.random() * this.TILE_COLORS.length)] + 
            'Tiles.png';

      this.SPECTATE_BOX_WIDTH = 60;

      this.TANK_COLORS = ['blueTank', 'greenTank', 'blackTank', 'beigeTank', 'orangeTank'];

      this.KILLED_YOURSELF_TEXT = "You killed yourself. Whoops.";
    },

    preload: function() {
      this.load.tilemap('map', this.TILEMAP_FILE, null, Phaser.Tilemap.TILED_JSON);
      this.load.image('tiles', this.TILESET_FILE);

      this.game.stage.disableVisibilityChange = true;
      this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
      this.sizeGame();

      var id;
      var resizer = function(gameState) {
        $(window).resize(function() {
          clearTimeout(id);
          id = setTimeout(function(gameState) {
            gameState = gameState[0]
            gameState.sizeGame()
          }, 100, [gameState]); 
        });          
      }
      resizer(this)
    },

    create: function() {
      var bomb, bombPositions, i, newTank, ref, type;
      network.message('gameIsActuallyPlaying');

      this.spectateMode = false;
      this.isAlive = true;
      this.wonGame = false;
      this.timeOut = false;
      this.game.data.state.playing = true;

      this.map = new GameMap(this).init();
      
      // Creating Tanks
      this.game.data.state.playerIDs.sort();
      this.allTanks = {};
      this.permanentTanks = {};
      for (var i = 0; i < this.game.data.state.playerIDs.length; i++) {
        var id = this.game.data.state.playerIDs[i];
        if (id && this.game.data.state.nicknames[id]) {
          var color = this.TANK_COLORS[i % this.TANK_COLORS.length];
          var tank = new Tank(this.game, 0, 0, color, id);
          this.allTanks[id] = tank;
          this.permanentTanks[id] = tank;
        }
      }
      this.permanentIDs = this.game.data.state.playerIDs.slice();
      this.myTank = this.allTanks[network.currentUser()];

      // Creating Bullets
      this.bullets = this.add.group();
      this.bullets.classType = window.Bullet;
      this.bullets.createMultiple(this.NUM_BULLETS, 'bullet');
      this.deadBullets = this.bullets.children.slice();

      this.activeMegaBullets = {};
      this.activeBullets = {};

      // Creating Bombs
      var bombPositions = this.game.data.state.bombPositions
      this.bombsMap = {};
      this.pickedUpBombs = [];
      for (var i = 0; i < bombPositions.length; i++) {
        var info = bombPositions[i];
        this.bombsMap[info.id] = new Bomb(this.game, info.x, info.y, info.id);
      }

      this.cursors = this.input.keyboard.createCursorKeys()
      this.world.setBounds(0, 0, this.WORLD_WIDTH, this.WORLD_HEIGHT)

      // Graphics
      this.sounds = new Sounds(this).init();
      this.infoBox = new InfoBox(this).init();
      this.minimap = new Minimap(this);
      this.scoreboard = new Scoreboard(this).init();
      this.gameBars = new GameBars(this).init();
      this.actionFeed = new ActionFeed(this).init();
      this.gameTimer = new GameTimer(this).init();
      this.overlays = new Overlays(this).init();
      this.smoothCamera = new Camera(this);

      this.sounds.playMusic();

      this.updateCounter = 0;

      var spacebar = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
      spacebar.onDown.add(this.spacebarPressed, this);
      spacebar.onUp.add(this.spacebarReleased, this);

      var bKey = this.game.input.keyboard.addKey(Phaser.KeyCode.B);
      bKey.onDown.add(this.bKeyPressed, this);
      bKey.onUp.add(this.bKeyReleased, this);

      var vKey = this.game.input.keyboard.addKey(Phaser.KeyCode.V);
      vKey.onDown.add(this.vKeyPressed, this);
      vKey.onUp.add(this.vKeyReleased, this);

      var cKey = this.game.input.keyboard.addKey(Phaser.KeyCode.C);
      cKey.onDown.add(this.cKeyPressed, this);
      cKey.onUp.add(this.cKeyReleased, this);
      
      $('#googleAd').hide();
    },

    update: function() {      
      this.updateCounter += 1;

      // 4 FPS
      if (this.updateCounter % 15 === 0) {
        this.minimap.refresh();
        this.infoBox.refresh();
        this.gameBars.refresh();
        this.gameTimer.refresh();
        this.actionFeed.refresh();
        this.scoreboard.refresh();
        this.checkOverlays();
      }

      // 60 FPS
      this.setTankNamePositions();        
      this.checkDirectionInput();
      this.smoothCamera.refresh();
    },

    shutdown: function() {
      network.message('goBackToLobby');
      this.sounds.destroyMusic();
    },

    checkDirectionInput: function() {
      if (this.spectateMode) {
        return this.smoothCamera.keyAdjustment()
      }

      if (this.cursors.up.isDown) {
        window.sendInput('upKeyDown')
      } else if (this.cursors.down.isDown) {
        window.sendInput('downKeyDown')
      } else if (this.cursors.left.isDown) {
        window.sendInput('leftKeyDown')
      } else if (this.cursors.right.isDown) {
        window.sendInput('rightKeyDown')
      }
    },

    spacebarPressed: function() { window.sendInput('spacebarDown'); },
    spacebarReleased: function() { window.sendInput('spacebarUp'); },
    bKeyPressed: function() { window.sendInput('bKeyDown'); },
    bKeyReleased: function() { window.sendInput('bKeyUp'); },
    vKeyPressed: function() { window.sendInput('vKeyDown'); },
    vKeyReleased: function() { window.sendInput('vKeyUp'); },
    cKeyPressed: function() { window.sendInput('cKeyDown'); },
    cKeyReleased: function() { window.sendInput('cKeyUp'); },

    startSpectateMode: function() {
      this.spectateMode = true;
      this.overlays.spectate()
      this.qKey = this.input.keyboard.addKey(Phaser.KeyCode.Q);
      this.qKey.onDown.add(this.shutdown, this, 0);
    },

    removeTank: function(id) {
      if (this.allTanks[id]) { 
        tank = this.allTanks[id];
        tank.die();
        this.actionFeed.appendNotice([tank.id, 'death']);
        delete this.allTanks[id];
      }
    },

    checkOverlays: function() {
      if (!this.overlays.active) {
        if (!this.isAlive && !this.spectateMode) {

          // The Tank has Died
          var name = this.game.data.state.nicknames[this.myTank.lastAssaulterID];
          var type = this.permanentTanks[this.myTank.lastAssaulterID].type;
          if (this.myTank.lastAssaulterID === this.myTank.id) {
            var diedText = this.KILLED_YOURSELF_TEXT;
          } else {
            var diedText = "You were killed by:";
          }
          this.overlays.died(diedText, name, type, this.game.data.state[this.myTank.id].score);

        } else if (this.timeOut) {
          // The timer ended
          this.overlays.timerEnded();
        } else if (this.wonGame) {
          // This client won the game
          this.overlays.wonGame();
        }
      }
    },

    setTankNamePositions: function() {
      if (this.game.data) {
        if (this.game.data.state) {
          if (this.game.data.state.playerIDs) {
            var ids = this.game.data.state.playerIDs;
            for (var i = 0; i < ids.length; i++) {
              var id = ids[i];
              var tank = this.allTanks[id];
              if (tank) {
                tank.shield.x = tank.x;
                tank.shield.y = tank.y;
                if (tank === this.myTank || !tank) { continue; }
                tank.healthBar.x = tank.x - 40;
                tank.healthBar.y = tank.y - 24;
                tank.healthBarColor.x = tank.x - 33;
                tank.healthBarColor.y = tank.y + 21;
                tank.text.x = tank.x;
                tank.text.y = tank.y + 38;
              }
            }            
          }
        }
      }
    },

    sizeGame: function() {
      var oldWidth = this.game.width; var oldHeight = this.game.height
      this.ASPECT_RATIO = window.innerWidth / window.innerHeight
      this.GAME_HEIGHT = 720;
      while ((this.GAME_HEIGHT * this.ASPECT_RATIO) >  this.GRID_SIZE * this.BOARD_SIZE) {
        this.GAME_HEIGHT -= 5
      }
      this.GAME_WIDTH = Math.floor(this.GAME_HEIGHT * this.ASPECT_RATIO)
      this.game.scale.setGameSize(this.GAME_WIDTH, this.GAME_HEIGHT);
      if (this.game.width !== oldWidth || this.game.height !== oldHeight) {
        if (this.map && this.map.layer) {
          this.map.layer.resize(this.game.width, this.game.height) 
        }
      }

      this.CANVAS_HEIGHT = window.innerHeight;
      this.CANVAS_WIDTH = this.CANVAS_HEIGHT * this.ASPECT_RATIO;
      while (this.CANVAS_WIDTH >= window.innerWidth) {
        this.CANVAS_HEIGHT = this.CANVAS_HEIGHT - 5;
        this.CANVAS_WIDTH = this.CANVAS_HEIGHT * this.ASPECT_RATIO;
      }
      this.game.scale.setUserScale(
        this.CANVAS_WIDTH / this.GAME_WIDTH, 
        this.CANVAS_HEIGHT / this.GAME_HEIGHT, 
        0, 0
      );

      if (this.infoBox) { this.infoBox.reposition() }
      if (this.actionFeed) { this.actionFeed.reposition() }
      if (this.scoreboard) { this.scoreboard.reposition() }
      if (this.gameBars) { this.gameBars.reposition() }
      if (this.gameTimer) { this.gameTimer.reposition() }
      if (this.overlays) { this.overlays.reposition() }
    }

  };
};
