/**
 * @file tank.js
 * Class definition for tanks
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function Tank(game, x, y, type, id) {
  this.TANK_OFFSET = game.state.getCurrentState().GRID_SIZE / 2;
  
  this.TARGET_ANGLE_TABLE = {};
  this.TARGET_ANGLE_TABLE[Phaser.DOWN] = 180;
  this.TARGET_ANGLE_TABLE[Phaser.UP] = 0;
  this.TARGET_ANGLE_TABLE[Phaser.RIGHT] = 90;
  this.TARGET_ANGLE_TABLE[Phaser.LEFT] = 270;

  Phaser.Sprite.call(this, game, x + this.TANK_OFFSET, y + this.TANK_OFFSET, type);
  game.add.existing(this);

  this.TURN_DURATION = 120;
  this.STARTING_HEALTH = 20;
  this.state = game.state.getCurrentState();
  this.id = id;
  this.width = 48;
  this.height = 48;
  this.anchor.setTo(0.5, 0.5);
  this.frame = 1;
  this.type = type;
  this.animations.add('move', [2, 1, 10, 9, 8, 7, 6, 5, 4, 3], 15, true);
  this.animating = false;
  this.currentDirection = Phaser.NONE;
  this.numShields = 2;
  this.hitStreak = 0;

  this.shield = this.game.add.graphics(this.x, this.y);
  this.shield.lineStyle(2, 0xFFFFFF, 0.6);
  this.shield.beginFill(0xFFFFFF, 0.3);
  this.shield.drawRoundedRect(-(this.width / 2) - 15, -(this.height / 2) - 15, 78, 78, 15);
  this.shield.endFill();
  this.shield.visible = false;

  // Creating Tank Text
  this.text = this.game.add.text(-100, -100, "Anonymous", {
    font: '12pt Orbitron',
    align: 'center',
    fill: 'white',
    stroke: 'rgba(0, 0, 0, 0)',
    strokeThickness: 3
  });
  this.text.anchor.setTo(0.5, 0.5);
  this.text.setText(this.game.data.state.nicknames[id])

  this.healthBar = this.game.add.graphics(-this.x - 40, -this.y - 24);
  this.healthBar.lineStyle(2, 0x7A7976);
  this.healthBar.beginFill(0xFFFFFF, 0.5);
  this.healthBar.drawRect(0, 0, 6, 45);
  this.healthBar.endFill();
  this.healthBarColor = this.game.add.graphics(-this.x - 34, -this.y + 21);
  this.healthBarColor.beginFill(0x00FF00, 1);
  this.healthBarColor.drawRect(0, 0, -6, -44);
  this.healthBarColor.endFill();

  this.updates = []

  this.refresh = function(x, y, currentDirection, moving) {
    if (currentDirection != this.currentDirection) {
      var tween = this.game.add.tween(this).to({
        angle: this._getAngle(currentDirection)
      }, this.TURN_DURATION, "Linear", true);
    }
    this.currentDirection = currentDirection;

    this.currentTween = this.game.add.tween(this).to({ x: x, y: y }, 25, "Linear", true);

    if (!moving) { this._stopAnimation(); }
    if (moving && !this.animating) { this._startAnimation(); }
  }

  this.die = function() {
    this.kill();
    this.text.kill();
    this.healthBar.kill();
    this.healthBarColor.kill();
    this.shield.kill();
    this._playKillPoof();
  }

  this.refreshHealthBar = function(amount) {
    var health = this.game.data.state[this.id].health

    this.healthBarColor.clear();

    if (health <= (this.STARTING_HEALTH * 0.2)) {
      this.healthBarColor.beginFill(0xFF0000, 1);
    } else if (health <= (this.STARTING_HEALTH * 0.5)) {
      this.healthBarColor.beginFill(0xFFFF00, 1);
    } else {
      this.healthBarColor.beginFill(0x00FF00, 1);
    }
    
    this.healthBarColor.drawRect(0, 0, -6, -44 * (health / this.STARTING_HEALTH));
    this.healthBarColor.endFill();
  }

  this._stopAnimation = function() {
    this.animations.stop('move');
    this.animating = false;
  };

  this._startAnimation = function() {
    this.animations.play('move');
    this.animating = true;
  }

  this.tileCoordinates = function() {
    var gameState, gridSize;
    gridSize = gameState = this.game.state.getCurrentState().GRID_SIZE;
    return [this.game.math.snapToFloor(this.x, gridSize) / gridSize,
            this.game.math.snapToFloor(this.y, gridSize) / gridSize];
  };

  this.activateShield = function() {
    this.shield.visible = true;
    this.game.time.events.add(5000, function() {
      this.shield.visible = false;
    }, this);
  }

  this._getAngle = function(to) {
    var diffTurnLeft, diffTurnRight, tankAngle, targetAngle, targetAngleNeg;
    tankAngle = this.game.math.radToDeg(this.rotation);
    targetAngle = this.TARGET_ANGLE_TABLE[to];
    targetAngleNeg = targetAngle - 360;
    diffTurnRight = this.game.math.difference(targetAngle, tankAngle);
    diffTurnLeft = this.game.math.difference(targetAngleNeg, tankAngle);
    if (diffTurnRight < diffTurnLeft) {
      return targetAngle;
    } else {
      return targetAngleNeg;
    }
  };

  this._playKillPoof = function() {
    var crumble = this.game.add.sprite(this.x, this.y, 'crumble')
    crumble.width = 128; crumble.height = 128
    crumble.anchor.setTo(0.5, 0.5)
    crumble.animations.add('crumbleAnimation', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 18, true)
    crumble.animations.play('crumbleAnimation', null, false, true);
  };

}

Tank.prototype = Object.create(Phaser.Sprite.prototype)
Tank.prototype.constructor = Tank
