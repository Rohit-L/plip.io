/**
 * @file bot.js
 * Class definition for the computer bot.
 *
 * @project plip.io
 * @author
 *   Rohit Lalchandani
 *   Pranay Kumar
 */
function Bot(gameState, position, color, id) {
  this.state = gameState;
  this.game = gameState.game;

  this.init = function() {
    this.MODES = ['OFFENSE', 'DEFENSE', 'MIDDLE']

    this.tank = new Tank(this.game, position[0], position[1], color, id);
    this.tank.bot = this;
    this.number = parseInt(id.substr(id.length - 1));

    this._counter = 0;
    this._mode = "OFFENSE";

    return this;
  }

  this.refresh = function() {
    this._counter += 1;

    if (!this.targetTank) {
      var allIDs = Object.keys(this.state.allTanks)
      var id = allIDs[Math.floor(Math.random() * allIDs.length)]
      this.targetTank = this.state.allTanks[id]
      while (this.targetTank === this.tank || !this.targetTank.alive) {
        id = allIDs[Math.floor(Math.random() * allIDs.length)]
        this.targetTank = this.state.allTanks[id]
      }
    }

    if (this._counter % 400 === 0) {
      var allIDs = Object.keys(this.state.allTanks)
      var id = allIDs[Math.floor(Math.random() * allIDs.length)]
      this.targetTank = this.state.allTanks[id]
      while (this.targetTank === this.tank || !this.targetTank.alive) {
        id = allIDs[Math.floor(Math.random() * allIDs.length)]
        this.targetTank = this.state.allTanks[id]
      }

      var rand = Math.random();
      if (rand <= 0.45) {
        this._mode = "OFFENSE";
      } else if (rand <= 0.9) {
        this.mode = "DEFENSE";
      } else {
        this.mode = "MIDDLE";
      }
    }

    if (this._counter % 5 === 0) {
      if (Math.random() > 0.800) { this._fire(); }
    }

    if (this._counter % 10 === 0) {
      this.chooseAction();
    }

    this.tank.setMarkerAndTurnPoints();
    this.tank.setAdjacentTiles();
    window.requestTurn([this.state, this.tank, true]);
  }

  this.chooseAction = function() {

    if (this._mode === "OFFENSE") {
      this._chaseOpponentTank();
    }

    if (this._mode === "DEFENSE") {
      this._runFromOpponentTank();
    }

    if (this._mode === "MIDDLE") {
      this._goToMiddle();
    }

    if (!this.tank.moving) {
      var arr = [];
      var threshold = 0.4;
      if (this.tank.adjacentTiles[Phaser.RIGHT].index === this.state.map.SAFE_TILE) {
        arr.push(Phaser.RIGHT);
      }
      if (this.tank.adjacentTiles[Phaser.DOWN].index === this.state.map.SAFE_TILE) {
        arr.push(Phaser.DOWN);
      }
      if (this.tank.adjacentTiles[Phaser.UP].index === this.state.map.SAFE_TILE) {
        arr.push(Phaser.UP);
      }
      if (this.tank.adjacentTiles[Phaser.LEFT].index === this.state.map.SAFE_TILE) {
        arr.push(Phaser.LEFT);
      }
      if (arr.length <= 2) {
        threshold = 0.8;
      }
      var rand = Math.random();
      if (rand <= threshold) {
        this._fire();
      }
      this.tank.futureDirection = this._chooseRandomDirection(arr);
    }
  }

  this._fire = function() {
    this.tank.fire(false, true);
  }

  /**************
   * Strategies *
   **************/
  this._chaseOpponentTank = function() {
    yDistance = Math.abs(this.tank.y - this.targetTank.y)
    xDistance = Math.abs(this.tank.x - this.targetTank.x)

    if (xDistance >= yDistance) {
      if (this.tank.x - this.targetTank.x > 0) {
        if (this.tank.adjacentTiles[Phaser.LEFT].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.LEFT
          return true
        }
      } else {
        if (this.tank.adjacentTiles[Phaser.RIGHT].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.RIGHT
          return true
        }
      }
    }

    if (yDistance >= xDistance) {
      if (this.tank.y - this.targetTank.y > 0) {
        if (this.tank.adjacentTiles[Phaser.UP].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.UP
          return true
        }
      } else {
        if (this.tank.adjacentTiles[Phaser.DOWN].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.DOWN
          return true
        }
      }
    }

    return false
  }

  this._runFromOpponentTank = function() {
    yDistance = Math.abs(this.tank.y - this.targetTank.y)
    xDistance = Math.abs(this.tank.x - this.targetTank.x)

    if (yDistance >= xDistance) {
      if (this.tank.x - this.targetTank.x > 0) {
        if (this.tank.adjacentTiles[Phaser.RIGHT].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.RIGHT
          return true
        }
      } else {
        if (this.tank.adjacentTiles[Phaser.LEFT].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.LEFT
          return true
        }
      }
    }

    if (xDistance >= yDistance) {
      if (this.tank.y - this.targetTank.y > 0) {
        if (this.tank.adjacentTiles[Phaser.DOWN].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.DOWN
          return true
        }
      } else {
        if (this.tank.adjacentTiles[Phaser.UP].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.UP
          return true
        }
      }
    }

    return false
  }

  this._goToMiddle = function() {
    var middle = (this.state.GRID_SIZE * this.state.BOARD_SIZE) / 2;

    yDistance = Math.abs(this.tank.y - middle)
    xDistance = Math.abs(this.tank.x - middle)

    if (xDistance >= yDistance) {
      if (this.tank.x - middle > 0) {
        if (this.tank.adjacentTiles[Phaser.LEFT].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.LEFT
          return true
        }
      } else {
        if (this.tank.adjacentTiles[Phaser.RIGHT].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.RIGHT
          return true
        }
      }
    }

    if (yDistance >= xDistance) {
      if (this.tank.y - middle > 0) {
        if (this.tank.adjacentTiles[Phaser.UP].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.UP
          return true
        }
      } else {
        if (this.tank.adjacentTiles[Phaser.DOWN].index === this.state.map.SAFE_TILE) {
          this.tank.futureDirection = Phaser.DOWN
          return true
        }
      }
    }

    return false
  };

  this._chooseRandomDirection = function(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
  }

}
