/* network client */
/* global module,define,require */
_ = this._

var removeKey = function(val, key, obj) {
  delete obj[key];
};

var createNetwork = function() {

  var uid;
  var socket;
  var events = {};
  var config = {};
  var timerEvents = {};
  var keyCodes = { '0': 'network-beginResponse', 
                   '1': 'network-lobbyMemberJoined', 
                   '2': 'network-joinedRoom',
                   '4': 'network-roomMemberJoined',
                   '5': 'network-lobbyMemberLeft',
                   '6': 'network-roomMemberLeft',
                   '7': 'network-roomCreated',
                   '8': 'network-roomDeleted',
                   '9': 'message-statistics',
                   '10': 'message-responseIDs',
                   '11': 'message-startGame',
                   '12': 'message-bombPositions',
                   '13': 'network-leftRoom',
                   '14': 'message-turnApproved',
                   '15': 'message-oppositeTurnIsValid',
                   '16': 'message-bombPlaced',
                   '17': 'message-bulletFired',
                   '18': 'message-tankUpdate',
                   '19': 'message-mapSelection',
                   '20': 'message-gameStats',
                   '21': 'message-addedToLobby',
                   '23': 'message-activateShield',
                   '24': 'message-destroyTile',
                   '25': 'message-botHost',
                   '26': 'message-bulletUpdate',
                   '27': 'message-killBullet',
                   '28': 'message-pickedUpBomb',
                   '29': 'message-placedBomb',
                   '30': 'message-damageWall',
                   '31': 'message-megaBulletUpdate',
                   '32': 'message-killMegaBullet',
                   '33': 'message-scoreUpdate',
                   '34': 'message-healthUpdate',
                   '35': 'message-bombCountUpdate',
                   '36': 'message-megaBulletCountUpdate',
                   '37': 'message-deadTankUpdate',
                   '38': 'message-timeOut',
                   '39': 'message-wonGame',
                   '40': 'message-actionUpdate',
                   '41': 'message-streakUpdate',
                   '42': 'message-timeUpdate',
                   '43': 'message-shieldActivated',
                   '44': 'message-preparingRoom' } // incoming
  var serverKeyCodes = { 'network-begin': 0, 
                         'message-connect': 1,
                         'message-requestIDs': 2,
                         'message-readyToPlay': 3,
                         'message-getBombPositions': 4,
                         'message-gameIsActuallyPlaying': 5,
                         'message-myTankInfo': 6,
                         'message-requestTurn': 7,
                         'message-turnCompleted': 8,
                         'message-bombPlaced': 9,
                         'message-bulletFired': 10,
                         'message-bulletInteraction': 11,
                         'message-goBackToLobby': 12,
                         'message-readyButtonPressed': 13,
                         'message-requestShield': 14,
                         'message-tileDestroyingNeeded': 15,
                         'message-transferBots': 17,
                         'message-input': 16 } // outgoing

  network = {

    configure: function(configArg) {

      // When specified, the `messages` option should trigger the complete
      // removal of any previously-bound message handlers.
      if (socket && configArg.messages) {
        _(config.messages).forEach(function(handler, name, messageHandlers) {
          socket.removeListener('message-' + name);
          network._off('message-' + name, handler);
        });
      }

      _.extend(config, configArg);

      if (socket) {
        network._applyConfig(config);
      }
    },

    _applyConfig: function(toApply) {

      _(toApply.messages).forEach(function(handler, name) {
        messages['message-' + name] = function(data) {
          network._trigger('message-' + name, data);
        };
        network._on('message-' + name, handler);
      });

      _(toApply.serverEvents).forEach(function(eventHandler, eventName) {
        network._on('network-' + eventName, eventHandler);
      });

      _.extend(timerEvents, toApply.timerEvents);
    },

    _on: function(event, handler) {
      if (events[event] === undefined) {
        events[event] = [];
      }
      events[event].push(handler);
    },

    _off: function(event, handler) {
      events[event] = _(events[event]).without(handler);
    },

    _trigger: function(event, arg) {
      if (events[event] !== undefined) {
        _.forEach(events[event], function(handler) {
          handler(arg);
        });
      }
    },

    run: function(url, options) {

      if (options === undefined) {
        options = {};
      }

      var ioOptions =  {
        'force new connection': true
      };

      if (options.socketIo) {
        ioOptions = _.extend(ioOptions, options.socketIo);
      }

      socket = new WebSocket(location.origin.replace(/^http/, 'ws'));
      socket.binaryType = "arraybuffer";

      socket.transmit = function(data) {
        if (socket.readyState === 1) {
          socket.send(data)
        }
      }

      socket.onmessage = function(event) {
        var keyCode = new DataView(event.data).getUint8(0)
        var message = keyCodes[keyCode]
        var data = event.data.slice(1)
        messages[message](data)
      }

      messages = {}

      socket.onopen = function() {
        if (uid === undefined) {

          // Send 'network-begin'
          data = new Uint8Array(1);
          data[0] = serverKeyCodes['network-begin'];
          socket.transmit(data);

        }
        else {
          socket.transmit('network-resume', {uid: uid});
        }
      };

      messages['disconnect'] = function() {
        network._trigger('network-disconnect');
      };

      messages['connecting'] = function() {
        network._trigger('network-connecting');
      };

      messages['network-roomMemberJoined'] = function(user) {
        network._trigger('network-roomMemberJoined', user);
      };

      messages['network-roomMemberLeft'] = function(user) {
        network._trigger('network-roomMemberLeft', user);
      };

      messages['network-lobbyMemberJoined'] = function(user) {
        network._trigger('network-lobbyMemberJoined', user);
      };

      messages['network-lobbyMemberLeft'] = function(user) {
        network._trigger('network-lobbyMemberLeft', user);
      };

      messages['network-joinedRoom'] = function(room) {
        network._trigger('network-joinedRoom', room);
      };

      messages['network-leftRoom'] = function(room) {
        network._trigger('network-leftRoom', room);
      };

      messages['network-roomCreated'] = function(rooms) {
        network._trigger('network-roomCreated', rooms);
      };

      messages['network-roomDeleted'] = function(rooms) {
        network._trigger('network-roomDeleted', rooms);
      };

      messages['network-beginResponse'] = function(data) {
        uid = new TextDecoder("utf-8").decode(new Uint8Array(data))
        network._trigger('network-begin');
      };

      messages['network-resumeResponse'] = function(data) {
        if (data.valid) {
          network._trigger('network-resume');
        }
        else {
          network._trigger('network-error', 'Could not resume.');
          network.stop();
        }
      };

      network._applyConfig(config);
    },

    stop: function() {
      this._disconnect();
      network._trigger('network-end');
      socket.removeAllListeners();
      _.forEach(events, removeKey);
      _.forEach(timerEvents, removeKey);
      socket = null;
      uid = undefined;
    },

    _disconnect: function() {
      socket.close();
    },

    _connect: function() {
      socket.socket.connect();
    },

    connected: function() {
      return socket && socket.readyState === 1; // TODO: Check native JS Websocket Docs for connected
    },

    currentUser: function() {
      return uid;
    },

    message: function(name, info) {
      if (this.connected()) {
        if (info) {
          info.setUint8(0, serverKeyCodes['message-' + name])
          socket.transmit(info);
        } else {
          info = new DataView(new ArrayBuffer(1))
          info.setUint8(0, serverKeyCodes['message-' + name])
          socket.transmit(info);
        }
      }
      else {
        throw 'Not connected.';
      }
    }

  };

  return network;

};

createNetwork();
